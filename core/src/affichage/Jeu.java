package affichage;

import java.util.ArrayList;

import client.chat.GestionChat;
import client.libSong.LibSong;
import client.monstre.Monstre;
import client.physique.CollisionCercle;
import client.physique.Position;
import client.terrain.Case;
import client.terrain.Terrain;
import client.tour.Tour;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;


public class Jeu extends ApplicationAdapter {

	//**************************************** Ajout chateau ****************************************
	private Stage stage;
	private SpriteBatch batch;
	private Camera camera;

	private Label lblscore;
	private Label lblargent;
	private Label lblmonstreRestant;
	private Label lblnomJeu;

	SpriteBatch premierText;
	BitmapFont font;
	GlyphLayout layout;
	LabelStyle lbstyle;
	Terrain terrain;
	GestionChat gestionChat;
	ShapeRenderer shape;

	Texture chateau;
	Texture monstre;
	Texture tour1;
	Texture tour2;
	Texture tour3;

	float longueurFenetre, largeurFenetre, longueurRect, largeurRect, coefLongueur, coefLargeur, x_tower, y_tower, x_monster, y_monster;
	int tailleMonstre = 6;
	int typeTour = 0;

	LibSong libSong;

	@Override
	public void create () {

		shape = new ShapeRenderer();
		batch = new SpriteBatch();

		chateau = new Texture(Gdx.files.internal("castle.png"));
		monstre = new Texture(Gdx.files.internal("MonstreSquelette.png"));
		tour1 = new Texture(Gdx.files.internal("TourBoisArcher.png"));
		tour2 = new Texture(Gdx.files.internal("TourPierreJetPierre.png"));
		tour3 = new Texture(Gdx.files.internal("TourPierreMortier.png"));
		premierText = new SpriteBatch();
		font = new BitmapFont();
		lbstyle = new LabelStyle(font, new Color (0,0,0,1));
		lblscore = new Label("Score : ", lbstyle);
		lblargent = new Label("Argent : "  , lbstyle);
		lblmonstreRestant = new Label("Monstre restant : ", lbstyle);
		lblnomJeu = new Label("Type tours: W,X,C", lbstyle);

		lblscore.setPosition(20, 480);
		lblargent.setPosition(410, 480);
		lblmonstreRestant.setPosition(270, 480);
		lblnomJeu.setPosition(120, 480);

		camera = new OrthographicCamera();
		stage = new Stage(new FitViewport(500, 500, camera));
		stage.addActor(lblscore);
		stage.addActor(lblargent);
		stage.addActor(lblmonstreRestant);
		stage.addActor(lblnomJeu);
		shape = new ShapeRenderer();

		libSong = new LibSong();

		LibSong.initSound("son/Sound/poserBatiment.wav", "Son Pose");

		LibSong.initMusic("son/Music/chaos sur terre.wav", "Musique fond");
		LibSong.initMusic("son/Music/Epic Greenland - Magical.mp3", "Musique fond 2");

		LibSong.playMusic();

		gestionChat = new GestionChat();
		gestionChat.lancerChat(gestionChat);


		initialiserJeu();

	}

	@Override
	public void render () {

		jouer();
		
		lblargent.setText("Argent : " + terrain.getGestionArgent().getMonArgent().getSolde());
		lblscore.setText("Score : " + terrain.getGestionScore().getMonScore().getScore());		
		lblmonstreRestant.setText("Monstre restant : " + terrain.getGestionVagues().getVagueActuelle().getNombreMonstres());

		Gdx.gl.glEnable(GL20.GL_BLEND);

		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glClearColor(1, 1, 1, 1);

		Gdx.graphics.getGL20().glClearColor( 1, 1, 1, 1 );
		Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );

		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();

		//Tracé du chateau
		batch.begin();
		batch.draw(chateau, (float)terrain.getGestionCases().getListeCases().get(terrain.getGestionCases().getListeCases().size() - 1).getPosition().getX(), (float)(Gdx.graphics.getHeight() - (terrain.getGestionCases().getListeCases().get(terrain.getGestionCases().getListeCases().size() - 1).getPosition().getY() + terrain.getGestionCases().getListeCases().get(0).getLargeur() - 3)), 18, 25);
		batch.end();
		
		// Tracé des lignes
		shape.begin(ShapeType.Line);
		shape.setColor(Color.BLACK);
		for(int i = 0; i < terrain.getGestionCases().getListeCases().size(); i++) {
			shape.rect((terrain.getGestionCases().getListeCases().get(i).getCelulle().getX() + 1) * (float)(longueurRect - 0.8), (terrain.getGestionCases().getListeCases().get(i).getCelulle().getY() + 1) * (float)(largeurRect - 0.8), (float)(longueurRect - 0.8), (float)(largeurRect - 0.8));
		}
		shape.end();	

		// Tracé des lasers
		shape.begin(ShapeType.Filled);
		shape.setColor(Color.RED);
		for (int i = 0; i < terrain.getGestionTour().getListeTours().size(); i++)
		{
			if(terrain.getGestionTour().getListeTours().get(i).getMonstre() != null) {
				shape.rectLine((terrain.getGestionTour().getListeTours().get(i).getPosition().getX()), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() + (float)(largeurRect / 2)), terrain.getGestionTour().getListeTours().get(i).getMonstre().getPosition().getX(), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getMonstre().getPosition().getY() + (float)(largeurRect / 4)), 10);
			}
		}
		shape.end();

		// Tracé des monstres
		batch.begin();
		for(int i = 0; i < terrain.getGestionMonstre().getListeMonstres().size(); i++){
			batch.draw(monstre, (float)terrain.getGestionMonstre().getListeMonstres().get(i).getPosition().getX() - (monstre.getWidth()/2), (float)(Gdx.graphics.getHeight() - (terrain.getGestionMonstre().getListeMonstres().get(i).getPosition().getY() + 3)));
		}
		batch.end();

		// Tracé des tours
		batch.begin();
		for(int i = 0; i < terrain.getGestionTour().getListeTours().size(); i++){
			switch (terrain.getGestionTour().getListeTours().get(i).getType()) {
			case 1:
				batch.draw(tour1, (int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)), tour1.getWidth()/2, tour1.getHeight());
				break;
			case 2:
				batch.draw(tour2, (int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)), tour2.getWidth()/2, tour2.getHeight());
				break;
			case 3:
				batch.draw(tour3, (int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)), tour3.getWidth()/2, tour3.getHeight());
				break;
			default:
				break;
			}
		}
		batch.end();

		// Tracé des cercles autour des tours
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		shape.begin(ShapeType.Filled);
		{
			shape.setColor(new Color(0,0,0,0.2f));
			for(int i = 0; i < terrain.getGestionTour().getListeTours().size(); ++i)
			{
				shape.circle((int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2 + (float)(longueurRect - 0.8)/2),Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)/2), terrain.getGestionTour().getListeTours().get(i).getCollisionneur().getRayon());
			}
		}
		shape.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);

	}

	@Override
	public void dispose(){
		LibSong.dispose();
	}

	public void initialiserJeu() {

		longueurRect = (float)Gdx.graphics.getWidth() / 40;
		largeurRect = (float)Gdx.graphics.getHeight() / 40;
		camera = new PerspectiveCamera();
		longueurFenetre = Gdx.graphics.getWidth();
		largeurFenetre = Gdx.graphics.getHeight();
		terrain = new Terrain();
		terrain.initialiserTerrain((int)longueurFenetre, (int)largeurFenetre, (int)longueurRect, (int)largeurRect);

	}

	public void jouer() {

		Terrain.getTemps().setTempsPasse(Gdx.graphics.getDeltaTime() * 1000);
		entreesUtilisateur();
		terrain.jouer(false);

	}

	public void entreesUtilisateur() {

		Gdx.input.setInputProcessor
		(
				new InputAdapter()
				{
					public boolean touchDown(int x, int y, int pointer, int button)
					{
						if(button == Input.Buttons.LEFT)
						{
							for(int i = 0; i < terrain.getGestionCases().getListeCases().size(); i++) {
								if(terrain.getGestionCases().getListeCases().get(i).getPosition().getX()  < Gdx.input.getX() && (terrain.getGestionCases().getListeCases().get(i).getPosition().getX() + longueurRect) > Gdx.input.getX()) {
									if(terrain.getGestionCases().getListeCases().get(i).getPosition().getY() < Gdx.input.getY() && (terrain.getGestionCases().getListeCases().get(i).getPosition().getY() + largeurRect) > Gdx.input.getY()) {
										if(terrain.getGestionCases().getListeCases().get(i).isOccupe() == false) {
											x_tower = terrain.getGestionCases().getListeCases().get(i).getPosition().getX();
											y_tower = terrain.getGestionCases().getListeCases().get(i).getPosition().getY();
											terrain.getGestionTour().spawnTour(typeTour, new Position(x_tower + longueurRect/2, y_tower + largeurRect/2 - 4), terrain, i);
											LibSong.playSound("Son Pose");
											terrain.setaChange(true);
										}
									}
								}
							}
						}
						return true;
					} 
				}
				);

		boolean keyWPressed = Gdx.input.isKeyPressed(Keys.W); 
		if (keyWPressed)
		{
			typeTour = 1;
		}

		boolean keyXPressed = Gdx.input.isKeyPressed(Keys.X); 
		if (keyXPressed)
		{
			typeTour = 2;
		}

		boolean keyCPressed = Gdx.input.isKeyPressed(Keys.C); 
		if (keyCPressed)
		{
			typeTour = 3;
		}

	}

	//**********************************************************************************************************************	


	//**************************************** Rassemblement ****************************************
	//	private Stage stage;
	//	private SpriteBatch batch;
	//	private Camera camera;
	//
	//	private Label lblscore;
	//	private Label lblargent;
	//	private Label lblmonstreRestant;
	//	private Label lblnomJeu;
	//
	//	SpriteBatch premierText;
	//	BitmapFont font;
	//	GlyphLayout layout;
	//	LabelStyle lbstyle;
	//	Terrain terrain;
	//	ShapeRenderer shape;
	//
	//	Texture monstre;
	//	Texture tour1;
	//	Texture tour2;
	//	Texture tour3;
	//
	//	float longueurFenetre, largeurFenetre, longueurRect, largeurRect, coefLongueur, coefLargeur, x_tower, y_tower, x_monster, y_monster;
	//	int tailleMonstre = 6;
	//	int typeTour = 0;
	//
	//	@Override
	//	public void create () {
	//
	//		shape = new ShapeRenderer();
	//		batch = new SpriteBatch();
	//
	//		monstre = new Texture(Gdx.files.internal("MonstreSquelette.png"));
	//		tour1 = new Texture(Gdx.files.internal("TourBoisArcher.png"));
	//		tour2 = new Texture(Gdx.files.internal("TourPierreJetPierre.png"));
	//		tour3 = new Texture(Gdx.files.internal("TourPierreMortier.png"));
	//		premierText = new SpriteBatch();
	//		font = new BitmapFont();
	//		lbstyle = new LabelStyle(font, new Color (0,0,0,1));
	//		lblscore = new Label("Score : ", lbstyle);
	//		lblargent = new Label("Argent : "  , lbstyle);
	//		lblmonstreRestant = new Label("Monstre restant : ", lbstyle);
	//		lblnomJeu = new Label("Type tours: W,X,C", lbstyle);
	//
	//		lblscore.setPosition(20, 480);
	//		lblargent.setPosition(410, 480);
	//		lblmonstreRestant.setPosition(270, 480);
	//		lblnomJeu.setPosition(120, 480);
	//
	//		camera = new OrthographicCamera();
	//		stage = new Stage(new FitViewport(500, 500, camera));
	//		stage.addActor(lblscore);
	//		stage.addActor(lblargent);
	//		stage.addActor(lblmonstreRestant);
	//		stage.addActor(lblnomJeu);
	//		shape = new ShapeRenderer();
	//
	//		initialiserJeu();
	//
	//	}
	//
	//	@Override
	//	public void render () {
	//
	//		lblargent.setText("Argent : " + terrain.getGestionArgent().getMonArgent().getSolde());
	//		lblscore.setText("Score : " + terrain.getGestionScore().getMonScore().getScore());		
	//
	//		jouer();
	//
	//		lblmonstreRestant.setText("Monstre restant : " + terrain.getGestionVagues().getVagueActuelle().getNombreMonstres());
	//
	//		Gdx.gl.glEnable(GL20.GL_BLEND);
	//
	//		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
	//		Gdx.gl.glClearColor(1, 1, 1, 1);
	//
	//		Gdx.graphics.getGL20().glClearColor( 1, 1, 1, 1 );
	//		Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
	//
	//		stage.act(Gdx.graphics.getDeltaTime());
	//		stage.draw();
	//
	//		// Tracé des lignes
	//		shape.begin(ShapeType.Line);
	//		shape.setColor(Color.BLACK);
	//		for(int i = 0; i < terrain.getGestionCases().getListeCases().size(); i++) {
	//			shape.rect((terrain.getGestionCases().getListeCases().get(i).getCelulle().getX() + 1) * (float)(longueurRect - 0.8), (terrain.getGestionCases().getListeCases().get(i).getCelulle().getY() + 1) * (float)(largeurRect - 0.8), (float)(longueurRect - 0.8), (float)(largeurRect - 0.8));
	//		}
	//		shape.end();	
	//
	//		// Tracé des lasers
	//		shape.begin(ShapeType.Filled);
	//		shape.setColor(Color.RED);
	//
	//		for (int i = 0; i < terrain.getGestionTour().getListeTours().size(); i++)
	//		{
	//			if(terrain.getGestionTour().getListeTours().get(i).getMonstre() != null) {
	//				shape.rectLine((terrain.getGestionTour().getListeTours().get(i).getPosition().getX()), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() + (float)(largeurRect / 2)), terrain.getGestionTour().getListeTours().get(i).getMonstre().getPosition().getX(), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getMonstre().getPosition().getY() + (float)(largeurRect / 4)), 10);
	//			}
	//		}
	//
	//		shape.end();
	//
	//		// Tracé des monstres
	//		batch.begin();
	//		for(int i = 0; i < terrain.getGestionMonstre().getListeMonstres().size(); i++){
	//			batch.draw(monstre, (float)terrain.getGestionMonstre().getListeMonstres().get(i).getPosition().getX() - (monstre.getWidth()/2), (float)(Gdx.graphics.getHeight() - (terrain.getGestionMonstre().getListeMonstres().get(i).getPosition().getY() + 3)));
	//		}
	//		batch.end();
	//
	//		// Tracé des tours
	//		batch.begin();
	//		for(int i = 0; i < terrain.getGestionTour().getListeTours().size(); i++){
	//			switch (terrain.getGestionTour().getListeTours().get(i).getType()) {
	//			case 1:
	//				batch.draw(tour1, (int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)), tour1.getWidth()/2, tour1.getHeight());
	//				break;
	//			case 2:
	//				batch.draw(tour2, (int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)), tour2.getWidth()/2, tour2.getHeight());
	//				break;
	//			case 3:
	//				batch.draw(tour3, (int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)), tour3.getWidth()/2, tour3.getHeight());
	//				break;
	//			default:
	//				break;
	//			}
	//		}
	//		batch.end();
	//
	//		// Tracé des cercles autour des tours
	//		Gdx.gl.glEnable(GL20.GL_BLEND);
	//		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
	//		shape.begin(ShapeType.Filled);
	//		{
	//			shape.setColor(new Color(0,0,0,0.2f));
	//			for(int i = 0; i < terrain.getGestionTour().getListeTours().size(); ++i)
	//			{
	//				shape.circle((int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2 + (float)(longueurRect - 0.8)/2),Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)/2), terrain.getGestionTour().getListeTours().get(i).getCollisionneur().getRayon());
	//			}
	//		}
	//		shape.end();
	//		Gdx.gl.glDisable(GL20.GL_BLEND);
	//
	//	}
	//
	//	public void initialiserJeu() {
	//
	//		longueurRect = (float)Gdx.graphics.getWidth() / 40;
	//		largeurRect = (float)Gdx.graphics.getHeight() / 40;
	//		camera = new PerspectiveCamera();
	//		longueurFenetre = Gdx.graphics.getWidth();
	//		largeurFenetre = Gdx.graphics.getHeight();
	//
	//		terrain = new Terrain();
	//		terrain.initialiserTerrain((int)longueurFenetre, (int)largeurFenetre, (int)longueurRect, (int)largeurRect);
	//
	//	}
	//
	//	public void jouer() {
	//
	//		Terrain.getTemps().setTempsPasse(Gdx.graphics.getDeltaTime() * 1000);
	//		entreesUtilisateur();
	//		terrain.jouer();
	//
	//	}
	//
	//	public void entreesUtilisateur() {
	//		
	//		Gdx.input.setInputProcessor
	//		(
	//				new InputAdapter()
	//				{
	//					public boolean touchDown(int x, int y, int pointer, int button)
	//					{
	//						if(button == Input.Buttons.LEFT)
	//						{
	//							for(int i = 0; i < terrain.getGestionCases().getListeCases().size(); i++) {
	//								if(terrain.getGestionCases().getListeCases().get(i).getPosition().getX()  < Gdx.input.getX() && (terrain.getGestionCases().getListeCases().get(i).getPosition().getX() + longueurRect) > Gdx.input.getX()) {
	//									if(terrain.getGestionCases().getListeCases().get(i).getPosition().getY() < Gdx.input.getY() && (terrain.getGestionCases().getListeCases().get(i).getPosition().getY() + largeurRect) > Gdx.input.getY()) {
	//										if(terrain.getGestionCases().getListeCases().get(i).isOccupe() == false) {
	//											x_tower = terrain.getGestionCases().getListeCases().get(i).getPosition().getX();
	//											y_tower = terrain.getGestionCases().getListeCases().get(i).getPosition().getY();
	//											terrain.getGestionTour().spawnTour(typeTour, new Position(x_tower + longueurRect/2, y_tower + largeurRect/2 - 4), terrain, i);
	//											terrain.getGestionCases().getListeCases().get(i).setOccupe(true);
	//											terrain.getGestionCases().getListeCases().get(i).setMaTour(terrain.getGestionTour().getListeTours().get(terrain.getGestionTour().getListeTours().size() - 1));
	//											terrain.setaChange(true);
	//										}
	//									}
	//								}
	//							}
	//						}
	//						return true;
	//					} 
	//				}
	//			);
	//		
	//		boolean keyWPressed = Gdx.input.isKeyPressed(Keys.W); 
	//		if (keyWPressed)
	//		{
	//			typeTour = 1;
	//		}
	//		
	//		boolean keyXPressed = Gdx.input.isKeyPressed(Keys.X); 
	//		if (keyXPressed)
	//		{
	//			typeTour = 2;
	//		}
	//		
	//		boolean keyCPressed = Gdx.input.isKeyPressed(Keys.C); 
	//		if (keyCPressed)
	//		{
	//			typeTour = 3;
	//		}
	//	
	//	}

	//**********************************************************************************************************************	

	//**************************************** Dernier merge pathfinding ****************************************

	//		private Viewport viewport;
	//		private Camera camera;
	//		int tailleMonstre = 6;
	//		Terrain terrain;
	//		
	//		float longueurFenetre, largeurFenetre, longueurRect, largeurRect, coefLongueur, coefLargeur, x_tower, y_tower, x_monster, y_monster;
	//		ShapeRenderer monstre, laser, radius, shape, tower, shapeRenderer;
	//		
	//		@Override
	//		public void create () {
	//			shapeRenderer = new ShapeRenderer();
	//			shape = new ShapeRenderer();
	//			tower = new ShapeRenderer();
	//			radius = new ShapeRenderer();
	//			monstre = new ShapeRenderer();
	//			laser = new ShapeRenderer();
	//			
	//			initialiserJeu();
	//		}
	//		
	//		@Override
	//		public void render () {
	//			
	//			jouer();
	//			
	//			Gdx.gl.glEnable(GL20.GL_BLEND);
	//
	//			Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
	//			Gdx.gl.glClearColor(1, 1, 1, 1);
	//
	//			Gdx.graphics.getGL20().glClearColor( 1, 1, 1, 1 );
	//			Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
	//
	//			shape.begin(ShapeType.Line);
	//			shape.setColor(Color.BLACK);
	//			for(int i = 0; i < terrain.getListeCases().size(); i++) {
	//				shape.rect((terrain.getListeCases().get(i).getCelulle().getX() + 1) * (float)(longueurRect - 0.8), (terrain.getListeCases().get(i).getCelulle().getY() + 1) * (float)(largeurRect - 0.8), (float)(longueurRect - 0.8), (float)(largeurRect - 0.8));
	//			}
	//			shape.end();
	//			
	//			tower.begin(ShapeType.Filled);
	//			tower.setColor(Color.BLACK);
	//			
	//			
	//			radius.begin(ShapeType.Filled);
	//			radius.setColor(new Color(0,0,0,0.2f));
	//
	//			laser.begin(ShapeType.Filled);
	//			laser.setColor(Color.RED);
	//			
	//			for (int i = 0; i < terrain.getListeTours().size(); i++)
	//			{
	//				tower.rect((int)(terrain.getListeTours().get(i).getPosition().getX() - longueurRect/2),Gdx.graphics.getHeight() - (terrain.getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)),(float)(longueurRect - 0.8),(float)(largeurRect - 0.8));
	//				radius.circle((int)(terrain.getListeTours().get(i).getPosition().getX() - longueurRect/2 + (float)(longueurRect - 0.8)/2),Gdx.graphics.getHeight() - (terrain.getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)/2), terrain.getListeTours().get(i).getCollisionneur().getRayon());
	//				for(int j=0; j< terrain.getListeMonstres().size(); j++) {
	//					if(terrain.getListeMonstres().get(j).getCollisionneur().detectionCollision(terrain.getListeTours().get(i).getPosition(), terrain.getListeTours().get(i).getCollisionneur().getRayon())) {
	//						terrain.getListeMonstres().get(j).retirerVie(2);
	//						terrain.getListeMonstres().get(j).vieZero();
	//						terrain.actualiserEtatMonstres();
	//					}
	//				}
	//			}
	//			
	//			tower.end();
	//			laser.end();
	//			radius.end();
	//			
	//			shapeRenderer.begin(ShapeType.Filled);
	//			shapeRenderer.setColor(Color.BLUE);
	//			for(int i = 0; i < terrain.getListeMonstres().size(); i++){
	//				shapeRenderer.circle((float)terrain.getListeMonstres().get(i).getPosition().getX(), (float)(Gdx.graphics.getHeight() - (terrain.getListeMonstres().get(i).getPosition().getY() + (tailleMonstre/2))), tailleMonstre);
	//			}
	//			shapeRenderer.end();
	//		}
	//		
	//		public void initialiserJeu() {
	//			
	//			longueurRect = (float)Gdx.graphics.getWidth() / 40;
	//			largeurRect = (float)Gdx.graphics.getHeight() / 40;
	//			camera = new PerspectiveCamera();
	//			longueurFenetre = Gdx.graphics.getWidth();
	//			largeurFenetre = Gdx.graphics.getHeight();
	//		    viewport = new StretchViewport(0, 0, camera);
	//		    
	//		    terrain = new Terrain();
	//		    terrain.initialiserTerrain((int)longueurFenetre, (int)largeurFenetre, (int)longueurRect, (int)largeurRect);
	//			
	//		}
	//		
	//		public void jouer() {
	//			
	//			entreesUtilisateur();
	//			Terrain.getTemps().setTempsPasse(Gdx.graphics.getDeltaTime() * 1000);
	//			terrain.jouer();
	//			System.out.println(Terrain.getTemps().getTempsPasse());
	//		}
	//		
	//		public void entreesUtilisateur() {
	//			
	//			Gdx.input.setInputProcessor
	//			(
	//					new InputAdapter()
	//					{
	//						public boolean touchDown(int x, int y, int pointer, int button)
	//						{
	//							if(button == Input.Buttons.LEFT)
	//							{
	//								for(int i = 0; i < terrain.getListeCases().size(); i++) {
	//									if(terrain.getListeCases().get(i).getPosition().getX()  < Gdx.input.getX() && (terrain.getListeCases().get(i).getPosition().getX() + longueurRect) > Gdx.input.getX()) {
	//										if(terrain.getListeCases().get(i).getPosition().getY() < Gdx.input.getY() && (terrain.getListeCases().get(i).getPosition().getY() + largeurRect) > Gdx.input.getY()) {
	//											if(terrain.getListeCases().get(i).isOccupe() == false) {
	//												x_tower = terrain.getListeCases().get(i).getPosition().getX();
	//												y_tower = terrain.getListeCases().get(i).getPosition().getY();
	//												terrain.getListeTours().add(new Tour(new CollisionCercle(30, new Position(x_tower + longueurRect/2, y_tower + largeurRect/2 - 4)), new Position(x_tower + longueurRect/2, y_tower + largeurRect/2 - 4)));
	//												terrain.getListeCases().get(i).setOccupe(true);
	//												terrain.setaChange(true);
	//											}
	//										}
	//									}
	//								}
	//							}
	//							else if(button == Input.Buttons.RIGHT) {
	//								System.out.println(Gdx.input.getX());
	//								System.out.println(Gdx.input.getY());
	//								for(int i = 0; i < terrain.getListeCases().size(); i++) {
	//									if(terrain.getListeCases().get(i).getPosition().getX()  < Gdx.input.getX() && (terrain.getListeCases().get(i).getPosition().getX() + longueurRect) > Gdx.input.getX()) {
	//										if(terrain.getListeCases().get(i).getPosition().getY() < Gdx.input.getY() && (terrain.getListeCases().get(i).getPosition().getY() + largeurRect) > Gdx.input.getY()) {
	//											if (terrain.getListeCases().get(i).isOccupe() == false) {
	//												Monstre monstre = new Monstre(new CollisionCercle(20, new Position(0, 0)), new Position(0, 0), terrain);
	//											}
	//										}
	//									}
	//								}
	//							}
	//							return true;
	//						} 
	//					}
	//				);
	//			
	//			boolean keySPressed = Gdx.input.isKeyPressed(Keys.S); 
	//			if ( keySPressed )
	//			{
	//				Monstre monstre = new Monstre(new CollisionCercle(10, new Position(0, 0)), new Position(0, 0), terrain);
	//				terrain.getGestionMonstre().spawnMonstre(monstre, terrain);
	//			}
	//			
	//			boolean keyCPressed = Gdx.input.isKeyPressed(Keys.C); 
	//			if ( keyCPressed )
	//			{
	//				terrain.setaChange(true);
	//			}
	//			
	//			boolean keyOPressed = Gdx.input.isKeyPressed(Keys.O); 
	//			if ( keyOPressed )
	//			{
	//				terrain.getListeCases().get(280).setOccupe(!terrain.getListeCases().get(280).isOccupe());
	//				terrain.setaChange(true);
	//			}
	//			
	//		}

	//**********************************************************************************************************************



	//**************************************** Gestion des monstres ****************************************
	//	
	//	private Viewport viewport;
	//	private Camera camera;
	//	int tailleMonstre = 6;
	//	Terrain terrain;
	//	
	//	float longueurFenetre, largeurFenetre, longueurRect, largeurRect, coefLongueur, coefLargeur, x_tower, y_tower, x_monster, y_monster;
	//	ArrayList<Tour> mesTours;
	//	ShapeRenderer monstre, laser, radius, shape, tower, shapeRenderer;
	//	
	//	@Override
	//	public void create () {
	//		shapeRenderer = new ShapeRenderer();
	//		
	//		shape = new ShapeRenderer();
	//		tower = new ShapeRenderer();
	//		radius = new ShapeRenderer();
	//		monstre = new ShapeRenderer();
	//		laser = new ShapeRenderer();
	//		mesTours = new ArrayList<Tour>();
	//		
	//		initialiserJeu();
	//	}
	//	
	//	@Override
	//	public void render () {
	//		
	//		jouer();
	//		
	//		Gdx.gl.glEnable(GL20.GL_BLEND);
	//
	//		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
	//		Gdx.gl.glClearColor(1, 1, 1, 1);
	//
	//		Gdx.graphics.getGL20().glClearColor( 1, 1, 1, 1 );
	//		Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
	//
	//		shape.begin(ShapeType.Line);
	//		shape.setColor(Color.BLACK);
	//		for(int i = 0; i < Terrain.getListeCases().size(); i++) {
	//			shape.rect((Terrain.getListeCases().get(i).getCelulle().getX() + 1) * (float)(longueurRect - 0.8), (Terrain.getListeCases().get(i).getCelulle().getY() + 1) * (float)(largeurRect - 0.8), (float)(longueurRect - 0.8), (float)(largeurRect - 0.8));
	//		}
	//		shape.end();
	//		
	//		tower.begin(ShapeType.Filled);
	//		tower.setColor(Color.BLACK);
	//		
	//		
	//		radius.begin(ShapeType.Filled);
	//		radius.setColor(new Color(0,0,0,0.2f));
	//
	//		laser.begin(ShapeType.Filled);
	//		laser.setColor(Color.RED);
	//		
	//		for (int i = 0; i < mesTours.size(); i++)
	//		{
	//			tower.rect((int)(mesTours.get(i).getPosition().getX() - longueurRect/2),Gdx.graphics.getHeight() - (mesTours.get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)),(float)(longueurRect - 0.8),(float)(largeurRect - 0.8));
	//			radius.circle((int)(mesTours.get(i).getPosition().getX() - longueurRect/2 + (float)(longueurRect - 0.8)/2),Gdx.graphics.getHeight() - (mesTours.get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)/2), mesTours.get(i).getCollisionneur().getRayon());
	//			for(int j=0; j< Terrain.getListeMonstres().size(); j++) {
	//				if(Terrain.getListeMonstres().get(j).getCollisionneur().detectionCollision(mesTours.get(i).getPosition(), mesTours.get(i).getCollisionneur().getRayon())) {
	//					Terrain.getListeMonstres().get(j).retirerVie(2);
	//					Terrain.getListeMonstres().get(j).vieZero();
	//					System.out.println(Terrain.getListeMonstres().get(j).getVie());
	//					terrain.actualiserEtatMonstres();
	//				}
	//			}
	//		}
	//		
	//		tower.end();
	//		laser.end();
	//		radius.end();
	//		
	//		shapeRenderer.begin(ShapeType.Filled);
	//		shapeRenderer.setColor(Color.BLUE);
	//		for(int i = 0; i < Terrain.getListeMonstres().size(); i++){
	//			shapeRenderer.circle((float)Terrain.getListeMonstres().get(i).getPosition().getX(), (float)(Gdx.graphics.getHeight() - (Terrain.getListeMonstres().get(i).getPosition().getY() + (tailleMonstre/2))), tailleMonstre);
	//		}
	//		shapeRenderer.end();
	//	}
	//	
	//	public void initialiserJeu() {
	//		
	//		longueurRect = (float)Gdx.graphics.getWidth() / 40;
	//		largeurRect = (float)Gdx.graphics.getHeight() / 40;
	//		camera = new PerspectiveCamera();
	//		longueurFenetre = Gdx.graphics.getWidth();
	//		largeurFenetre = Gdx.graphics.getHeight();
	//	    viewport = new StretchViewport(0, 0, camera);
	//	    
	//	    terrain = new Terrain();
	//	    terrain.initialiserTerrain((int)longueurFenetre, (int)largeurFenetre, (int)longueurRect, (int)largeurRect);
	//		
	//	}
	//	
	//	public void jouer() {
	//		
	//		entreesUtilisateur();
	//		terrain.jouer();
	//		
	//	}
	//	
	//	public void entreesUtilisateur() {
	//		
	//		Gdx.input.setInputProcessor
	//		(
	//				new InputAdapter()
	//				{
	//					public boolean touchDown(int x, int y, int pointer, int button)
	//					{
	//						if(button == Input.Buttons.LEFT)
	//						{
	//							for(int i = 0; i < Terrain.getListeCases().size(); i++) {
	//								if(Terrain.getListeCases().get(i).getPosition().getX()  < Gdx.input.getX() && (Terrain.getListeCases().get(i).getPosition().getX() + longueurRect) > Gdx.input.getX()) {
	//									if(Terrain.getListeCases().get(i).getPosition().getY() < Gdx.input.getY() && (Terrain.getListeCases().get(i).getPosition().getY() + largeurRect) > Gdx.input.getY()) {
	//										if(Terrain.getListeCases().get(i).isOccupe() == false) {
	//											x_tower = Terrain.getListeCases().get(i).getPosition().getX();
	//											y_tower = Terrain.getListeCases().get(i).getPosition().getY();
	//											mesTours.add(new Tour(new CollisionCercle(30, new Position(x_tower + longueurRect/2, y_tower + largeurRect/2 - 4)), new Position(x_tower + longueurRect/2, y_tower + largeurRect/2 - 4)));
	//											Terrain.getListeCases().get(i).setOccupe(true);
	//											Terrain.setaChange(true);
	//											System.out.println("Mise en place d'une tour : " + (x_tower + longueurRect/2) + " | " + (y_tower + largeurRect/2 - 4));
	//										}
	//									}
	//								}
	//							}
	//						}
	//						else if(button == Input.Buttons.RIGHT) {
	//							System.out.println(Gdx.input.getX());
	//							System.out.println(Gdx.input.getY());
	//							for(int i = 0; i < Terrain.getListeCases().size(); i++) {
	//								if(Terrain.getListeCases().get(i).getPosition().getX()  < Gdx.input.getX() && (Terrain.getListeCases().get(i).getPosition().getX() + longueurRect) > Gdx.input.getX()) {
	//									if(Terrain.getListeCases().get(i).getPosition().getY() < Gdx.input.getY() && (Terrain.getListeCases().get(i).getPosition().getY() + largeurRect) > Gdx.input.getY()) {
	//										if (Terrain.getListeCases().get(i).isOccupe() == false) {
	//											Monstre monstre = new Monstre(new CollisionCercle(20, new Position(0, 0)), new Position(0, 0));
	//											//terrain.getGestionMonstre().spawnMonstre(monstre);
	//										}
	//									}
	//								}
	//							}
	//						}
	//						return true;
	//					} 
	//				}
	//				);
	//		
	//		boolean keySPressed = Gdx.input.isKeyPressed(Keys.S); 
	//		if ( keySPressed )
	//		{
	//			Monstre monstre = new Monstre(new CollisionCercle(10, new Position(0, 0)), new Position(0, 0));
	//			terrain.getGestionMonstre().spawnMonstre(monstre);
	//			System.out.println("Spawn d'un monstre");
	//		}
	//		
	//		boolean keyCPressed = Gdx.input.isKeyPressed(Keys.C); 
	//		if ( keyCPressed )
	//		{
	//			Terrain.setaChange(true);
	//		}
	//		
	//		boolean keyOPressed = Gdx.input.isKeyPressed(Keys.O); 
	//		if ( keyOPressed )
	//		{
	//			Terrain.getListeCases().get(280).setOccupe(!Terrain.getListeCases().get(280).isOccupe());
	//			Terrain.setaChange(true);
	//		}
	//		
	//	}
	//		


	//	*************************************************	Fin gestion monstre	****************************************************


	//	private Viewport viewport;
	//	private Camera camera;
	//	int tailleMonstre = 6;
	//	Terrain terrain;
	//	
	//	float longueurFenetre, largeurFenetre, longueurRect, largeurRect, coefLongueur, coefLargeur, x_tower, y_tower, x_monster, y_monster;
	//	ArrayList<Tour> mesTours;
	//	ShapeRenderer monstre, laser, radius, shape, tower, shapeRenderer;
	//	
	//	@Override
	//	public void create () {
	//		shapeRenderer = new ShapeRenderer();
	//		
	//		shape = new ShapeRenderer();
	//		tower = new ShapeRenderer();
	//		radius = new ShapeRenderer();
	//		monstre = new ShapeRenderer();
	//		laser = new ShapeRenderer();
	//		mesTours = new ArrayList<Tour>();
	//		
	//		initialiserJeu();
	//	}
	//	
	//	@Override
	//	public void render () {
	//		
	//		jouer();
	//		
	//		Gdx.gl.glEnable(GL20.GL_BLEND);
	//
	//		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
	//		Gdx.gl.glClearColor(1, 1, 1, 1);
	//
	//		Gdx.graphics.getGL20().glClearColor( 1, 1, 1, 1 );
	//		Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
	//
	//		shape.begin(ShapeType.Line);
	//		shape.setColor(Color.BLACK);
	//		for(int i = 0; i < Terrain.getListeCases().size(); i++) {
	//			shape.rect((Terrain.getListeCases().get(i).getCelulle().getX() + 1) * (float)(longueurRect - 0.8), (Terrain.getListeCases().get(i).getCelulle().getY() + 1) * (float)(largeurRect - 0.8), (float)(longueurRect - 0.8), (float)(largeurRect - 0.8));
	//		}
	//		shape.end();
	//		
	//		tower.begin(ShapeType.Filled);
	//		tower.setColor(Color.BLACK);
	//		
	//		
	//		radius.begin(ShapeType.Filled);
	//		radius.setColor(new Color(0,0,0,0.2f));
	//
	//		laser.begin(ShapeType.Filled);
	//		laser.setColor(Color.RED);
	//		
	//		for (int i = 0; i < mesTours.size(); i++)
	//		{
	//			tower.rect((int)(mesTours.get(i).getPosition().getX() - longueurRect/2),Gdx.graphics.getHeight() - (mesTours.get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)),(float)(longueurRect - 0.8),(float)(largeurRect - 0.8));
	//			radius.circle((int)(mesTours.get(i).getPosition().getX() - longueurRect/2 + (float)(longueurRect - 0.8)/2),Gdx.graphics.getHeight() - (mesTours.get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)/2), mesTours.get(i).getCollisionneur().getRayon());
	//			for(int j=0; j< Terrain.getListeMonstres().size(); j++) {
	//				if(Terrain.getListeMonstres().get(j).getCollisionneur().detectionCollision(mesTours.get(i).getPosition(), mesTours.get(i).getCollisionneur().getRayon())) {
	//					Terrain.getListeMonstres().get(j).retirerVie(2);
	//					Terrain.getListeMonstres().get(j).vieZero();
	//					System.out.println(Terrain.getListeMonstres().get(j).getVie());
	//					terrain.actualiserEtatMonstres();
	//				}
	//			}
	//		}
	//		
	//		tower.end();
	//		laser.end();
	//		radius.end();
	//		
	//		shapeRenderer.begin(ShapeType.Filled);
	//		shapeRenderer.setColor(Color.BLUE);
	//		for(int i = 0; i < Terrain.getListeMonstres().size(); i++){
	//			shapeRenderer.circle((float)Terrain.getListeMonstres().get(i).getPosition().getX(), (float)(Gdx.graphics.getHeight() - (Terrain.getListeMonstres().get(i).getPosition().getY() + (tailleMonstre/2))), tailleMonstre);
	//		}
	//		shapeRenderer.end();
	//	}
	//	
	//	public void initialiserJeu() {
	//		
	//		longueurRect = (float)Gdx.graphics.getWidth() / 40;
	//		largeurRect = (float)Gdx.graphics.getHeight() / 40;
	//		camera = new PerspectiveCamera();
	//		longueurFenetre = Gdx.graphics.getWidth();
	//		largeurFenetre = Gdx.graphics.getHeight();
	//	    viewport = new StretchViewport(0, 0, camera);
	//	    
	//	    terrain = new Terrain();
	//	    terrain.initialiserTerrain((int)longueurFenetre, (int)largeurFenetre, (int)longueurRect, (int)largeurRect);
	//		
	//	}
	//	
	//	public void jouer() {
	//		
	//		entreesUtilisateur();
	//		terrain.jouer();
	//		
	//	}
	//	
	//	public void entreesUtilisateur() {
	//		
	//		Gdx.input.setInputProcessor
	//		(
	//				new InputAdapter()
	//				{
	//					public boolean touchDown(int x, int y, int pointer, int button)
	//					{
	//						if(button == Input.Buttons.LEFT)
	//						{
	//							for(int i = 0; i < Terrain.getListeCases().size(); i++) {
	//								if(Terrain.getListeCases().get(i).getPosition().getX()  < Gdx.input.getX() && (Terrain.getListeCases().get(i).getPosition().getX() + longueurRect) > Gdx.input.getX()) {
	//									if(Terrain.getListeCases().get(i).getPosition().getY() < Gdx.input.getY() && (Terrain.getListeCases().get(i).getPosition().getY() + largeurRect) > Gdx.input.getY()) {
	//										if(Terrain.getListeCases().get(i).isOccupe() == false) {
	//											x_tower = Terrain.getListeCases().get(i).getPosition().getX();
	//											y_tower = Terrain.getListeCases().get(i).getPosition().getY();
	//											mesTours.add(new Tour(new CollisionCercle(30, new Position(x_tower + longueurRect/2, y_tower + largeurRect/2 - 4)), new Position(x_tower + longueurRect/2, y_tower + largeurRect/2 - 4)));
	//											Terrain.getListeCases().get(i).setOccupe(true);
	//											Terrain.setaChange(true);
	//										}
	//									}
	//								}
	//							}
	//						}
	//						else if(button == Input.Buttons.RIGHT) {
	//							System.out.println(Gdx.input.getX());
	//							System.out.println(Gdx.input.getY());
	//							for(int i = 0; i < Terrain.getListeCases().size(); i++) {
	//								if(Terrain.getListeCases().get(i).getPosition().getX()  < Gdx.input.getX() && (Terrain.getListeCases().get(i).getPosition().getX() + longueurRect) > Gdx.input.getX()) {
	//									if(Terrain.getListeCases().get(i).getPosition().getY() < Gdx.input.getY() && (Terrain.getListeCases().get(i).getPosition().getY() + largeurRect) > Gdx.input.getY()) {
	//										if (Terrain.getListeCases().get(i).isOccupe() == false) {
	//											Monstre monstre = new Monstre(new CollisionCercle(20, new Position(0, 0)), new Position(0, 0));
	//											//terrain.getGestionMonstre().spawnMonstre(monstre);
	//										}
	//									}
	//								}
	//							}
	//						}
	//						return true;
	//					} 
	//				}
	//				);
	//		
	//		boolean keySPressed = Gdx.input.isKeyPressed(Keys.S); 
	//		if ( keySPressed )
	//		{
	//			Monstre monstre = new Monstre(new CollisionCercle(10, new Position(0, 0)), new Position(0, 0));
	//			terrain.getGestionMonstre().spawnMonstre(monstre);
	//			System.out.println("Spawn d'un monstre");
	//		}
	//		
	//		boolean keyCPressed = Gdx.input.isKeyPressed(Keys.C); 
	//		if ( keyCPressed )
	//		{
	//			Terrain.setaChange(true);
	//		}
	//		
	//		boolean keyOPressed = Gdx.input.isKeyPressed(Keys.O); 
	//		if ( keyOPressed )
	//		{
	//			Terrain.getListeCases().get(280).setOccupe(!Terrain.getListeCases().get(280).isOccupe());
	//			Terrain.setaChange(true);
	//		}
	//		
	//	}

	//**********************************************************************************************************************


	//**************************************** Table de jeu avec les cases v2****************************************

	//	ShapeRenderer shape;
	//	float longueurFenetre;
	//	float largeurFenetre;
	//	float longueurRect;
	//	float largeurRect;
	//	float coefLongueur;
	//	float coefLargeur;
	//	private Viewport viewport;
	//	private Camera camera;
	//	ArrayList<Case> mesCases = new ArrayList<Case>();
	//
	//	@Override
	//	public void create () {
	//		shape = new ShapeRenderer();
	//		longueurRect = (float)Gdx.graphics.getWidth() / 40;
	//		largeurRect = (float)Gdx.graphics.getHeight() / 40;
	//		camera = new PerspectiveCamera();
	//		longueurFenetre = Gdx.graphics.getWidth();
	//		largeurFenetre = Gdx.graphics.getHeight();
	//	    viewport = new StretchViewport(0, 0, camera);
	//	}
	//	@Override
	//	public void render () {
	//		Gdx.graphics.getGL20().glClearColor( 1, 1, 1, 1 );
	//		Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT |
	//				GL20.GL_DEPTH_BUFFER_BIT );
	//		shape.begin(ShapeType.Line);
	//		shape.setColor(Color.BLACK);
	//
	//		for (int i = 1; i < 41; i++) {
	//			for(int j = 1; j < 41; j++) {
	//				shape.rect(i * (float)(longueurRect - 0.8), j * (float)(largeurRect - 0.8), (float)(longueurRect - 0.8), (float)(largeurRect - 0.8));
	//				mesCases.add(new Case(new Position(i * (int)(longueurRect - 0.8), j * (int)(largeurRect - 0.8))));
	//			}
	//		}
	//		
	//		//System.out.println(mesCases.get(13).getPosition().getX());
	//		//System.out.println(mesCases.get(13).getPosition().getY());
	//		
	//		boolean leftPressed = Gdx.input.isButtonPressed(Input.Buttons.LEFT);
	//		if ( leftPressed )
	//		{
	//			System.out.println("kill : "+ (Gdx.input.getX()/coefLongueur) +" "+(Gdx.input.getY()/coefLargeur));
	//		}
	//		Gdx.input.setInputProcessor(new	InputAdapter () {
	//			public boolean touchDown (int x, int y, int pointer, int button) {
	//				if ( button == Input.Buttons.RIGHT){
	//					System.out.println("Spawn : "+ (Gdx.input.getX()/coefLongueur) +" "+(Gdx.input.getY()/coefLargeur));
	//				}
	//				return true;
	//			}
	//		});
	//		shape.end();
	//	}
	//	
	//	@Override
	//	public void resize(int width, int height) {
	//		coefLongueur = Gdx.graphics.getWidth() / longueurFenetre;
	//		coefLargeur = Gdx.graphics.getHeight() / largeurFenetre;
	//	    viewport.update(width, height, true);
	//	}

	//**********************************************************************************************************************



	//**************************************** Table de jeu avec les cases ****************************************

	//	ShapeRenderer shape;
	//	float longueurRect;
	//	float largeurRect;
	//	private Viewport viewport;
	//	private Camera camera;
	//
	//	@Override
	//	public void create () {
	//		shape = new ShapeRenderer();
	//		longueurRect = (float)Gdx.graphics.getWidth() / 40;
	//		largeurRect = (float)Gdx.graphics.getHeight() / 40;
	//		camera = new PerspectiveCamera();
	//	    viewport = new StretchViewport(800, 480, camera);
	//	}
	//	@Override
	//	public void render () {
	//		Gdx.graphics.getGL20().glClearColor( 1, 1, 1, 1 );
	//		Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT |
	//				GL20.GL_DEPTH_BUFFER_BIT );
	//		shape.begin(ShapeType.Line);
	//		shape.setColor(Color.BLACK);
	//
	//		for (int i=1; i < 41; i++) {
	//			for(int j = 1; j < 41; j++) {
	//				shape.rect(i * (float)(longueurRect - 0.8), j * (float)(largeurRect - 0.8), (float)(longueurRect - 0.8), (float)(largeurRect - 0.8));
	//			}
	//		}
	//
	//		shape.end();
	//
	////		boolean leftPressed = Gdx.input.isButtonPressed(Input.Buttons.LEFT);
	////		if ( leftPressed )
	////		{
	////			System.out.println("kill : "+ Gdx.input.getX() +" "+Gdx.input.getY());
	////		}
	////		Gdx.input.setInputProcessor(new
	////				InputAdapter () {
	////			public boolean touchDown (int x, int y, int pointer, int button) {
	////				if ( button == Input.Buttons.RIGHT){
	////					System.out.println("Spawn : "+ Gdx.input.getX() +" "+Gdx.input.getY());
	////				}
	////				return true;
	////			}
	////		});
	//
	//	}
	//	
	//	@Override
	//	public void resize(int width, int height) {
	//	    viewport.update(width, height, true);
	//	}

	//**********************************************************************************************************************


	//**************************************** Affiche cercle avec position alÃ©atoire  ****************************************
	// Affiche cercle avec position alÃ©atoire 

	/*SpriteBatch batch;
		ShapeRenderer shapeRenderer;
		CollisionCercle cercle;
		public ArrayList<CollisionCercle> collCercle;
		int i=0;

		@Override
		public void create () {
			//batch = new SpriteBatch();
			shapeRenderer = new ShapeRenderer();
			collCercle = new ArrayList<CollisionCercle>();
			for(i=0; i<=20; i++){
				collCercle.add(new CollisionCercle(10, new Position(Math.random()*(500-1), Math.random()*(500-1) ) ) );
			}

		}



		@Override
		public void render () {

			Gdx.gl.glClearColor(1, 0, 0, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);



			shapeRenderer.begin(ShapeType.Filled);
			shapeRenderer.setColor(Color.BLUE);
			for(int i = 0; i < collCercle.size(); i++){
				shapeRenderer.circle((float)collCercle.get(i).getPosition().getX(), (float)collCercle.get(i).getPosition().getY(), 10);
			}

			shapeRenderer.end();
		}*/

	//**********************************************************************************************************************

	//**************************************** Code permettant d'afficher le terrain ****************************************
	//	 Texture terrain;
	//	 TextureRegion monTerrain;
	//	 SpriteBatch batch;

	//	@Override
	//	public void create () {
	//		terrain = new Texture(Gdx.files.internal("Terrain AAA.png"));
	//		monTerrain = new TextureRegion(terrain, 0, 0, 1024, 1024);
	//		batch = new SpriteBatch();
	//	}

	//	@Override
	//	public void render () {
	//		Gdx.gl.glClearColor(1, 1, 1, 1);
	//		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	//		batch.begin();
	//		batch.draw(monTerrain, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	//		batch.end();
	//	}

	//	@Override
	//	public void resize(int width, int height) {
	//		batch.getProjectionMatrix().setToOrtho2D(0, 0, width, height);
	//	}
	//**********************************************************************************************************************

}
