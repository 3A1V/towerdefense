package affichage;

import client.chat.GestionChat;
import client.libSong.LibSong;
import client.monstre.Monstre;
import client.monstre.Vague;
import client.physique.CollisionCercle;
import client.physique.Position;
import client.reseau.Client;
import client.reseau.ReseauGUI;
import client.terrain.Terrain;
import client.tour.Tour;

import java.util.ArrayList;

import serveur.reseau.Serveur;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class JeuReseau extends ApplicationAdapter {
	
	//**************************************** Retirer vie ****************************************
	private Stage stage;
	private SpriteBatch batch;
	private Camera camera;

	private Label lblgameover;
	private Label lblscore;
	private Label lblargent;
	private Label lblmonstreRestant;
	private Label lblnomJeu;

	SpriteBatch premierText;
	BitmapFont font;
	GlyphLayout layout;
	LabelStyle lbstyle;
	LabelStyle lbstyle_gameover;
	Terrain terrain;
	GestionChat gestionChat;
	ShapeRenderer shape;
	Texture mapJeu;
	Texture chateau;
	Texture monstre;
	Texture tour1;
	Texture tour2;
	Texture tour3;
	String castle = "castle.png";

	float longueurFenetre, largeurFenetre, longueurRect, largeurRect, coefLongueur, coefLargeur, x_tower, y_tower, x_monster, y_monster;
	int tailleMonstre = 6;
	int typeTour = 0;

	LibSong libSong;
	
	ReseauGUI reseauGUI;
	boolean isServer;

	public JeuReseau(boolean isServer) {
		this.isServer = isServer;
	}

	@Override
	public void create () {
		shape = new ShapeRenderer();
		batch = new SpriteBatch();
		mapJeu = new Texture(Gdx.files.internal("map_jeu.png"));
		chateau = new Texture(Gdx.files.internal(castle));
		monstre = new Texture(Gdx.files.internal("MonstreSquelette.png"));
		tour1 = new Texture(Gdx.files.internal("TourBoisArcher.png"));
		tour2 = new Texture(Gdx.files.internal("TourPierreJetPierre.png"));
		tour3 = new Texture(Gdx.files.internal("TourPierreMortier.png"));
		premierText = new SpriteBatch();
		font = new BitmapFont();
		lbstyle = new LabelStyle(font, new Color (0,0,0,1));
		lbstyle_gameover = new LabelStyle(font, new Color (1,0,0,1));
		lblscore = new Label("Score : ", lbstyle);
		lblgameover = new Label("", lbstyle_gameover);
		lblargent = new Label("Argent : "  , lbstyle);
		lblmonstreRestant = new Label("Monstre restant : ", lbstyle);
		lblnomJeu = new Label("Type tours: W,X,C", lbstyle);

		lblscore.setPosition(20, 480);
		lblgameover.setPosition(200, 250);
		lblargent.setPosition(410, 480);
		lblmonstreRestant.setPosition(270, 480);
		lblnomJeu.setPosition(120, 480);

		camera = new OrthographicCamera();
		stage = new Stage(new FitViewport(500, 500, camera));
		stage.addActor(lblscore);
		stage.addActor(lblargent);
		stage.addActor(lblmonstreRestant);
		stage.addActor(lblnomJeu);
		shape = new ShapeRenderer();
		stage.addActor(lblgameover);
		libSong = new LibSong();

		LibSong.initSound("son/Sound/poserBatiment.wav", "Son Pose");

		LibSong.initMusic("son/Music/chaos sur terre.wav", "Musique fond");
		LibSong.initMusic("son/Music/Epic Greenland - Magical.mp3", "Musique fond 2");

		LibSong.playMusic();

		gestionChat = new GestionChat();
		gestionChat.lancerChat(gestionChat);


		initialiserJeu();

	}

	@Override
	public void render () {		
		jouer();

		lblargent.setText("Argent : " + terrain.getGestionArgent().getMonArgent().getSolde());
		lblscore.setText("Score : " + terrain.getGestionScore().getMonScore().getScore());		
		//lblmonstreRestant.setText("Monstre restant : " + terrain.getGestionVagues().getVagueActuelle().getNombreMonstres());

		Gdx.gl.glEnable(GL20.GL_BLEND);

		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glClearColor(1, 1, 1, 1);

		Gdx.graphics.getGL20().glClearColor( 1, 1, 1, 1 );
		Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );


		

		// Tracé de la map
		batch.begin();
		batch.draw(mapJeu, terrain.getGestionCases().getListeCases().get(38).getPosition().getX(), Gdx.graphics.getHeight() - terrain.getGestionCases().getListeCases().get(39).getPosition().getY() - terrain.getGestionCases().getListeCases().get(0).getLargeur(), 608, 450);
		batch.end();
		//Tracé du chateau
		batch.begin();
		batch.draw(chateau, (float)terrain.getGestionCases().getListeCases().get(terrain.getGestionCases().getListeCases().size() - 1).getPosition().getX(), (float)(Gdx.graphics.getHeight() - (terrain.getGestionCases().getListeCases().get(terrain.getGestionCases().getListeCases().size() - 1).getPosition().getY() + terrain.getGestionCases().getListeCases().get(0).getLargeur() - 3)), 18, 25);
		batch.end();
		System.out.println(terrain.getChateau().getVie());
		if(!terrain.getChateau().isVivant()) {
			lblgameover.setText("GAME OVER");
		}
		
		if(!terrain.getChateau().isVivant()) {
			castle = "castle_fire.png";
		}
		
		// Tracé du cercle autour du chateau
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		shape.begin(ShapeType.Filled);
		{
			shape.setColor(new Color(0,0,0,0.2f));
			shape.circle((int)(terrain.getChateau().getPosition().getX() - longueurRect/2 + (float)(longueurRect - 0.8)/2),Gdx.graphics.getHeight() - (terrain.getChateau().getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)/2), terrain.getChateau().getCollisionneur().getRayon());
		}
		shape.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);
		// Tracé des lignes
		shape.begin(ShapeType.Line);
		shape.setColor(Color.BLACK);
		for(int i = 0; i < terrain.getGestionCases().getListeCases().size(); i++) {
			shape.rect((terrain.getGestionCases().getListeCases().get(i).getCelulle().getX() + 1) * (float)(longueurRect - 0.8), (terrain.getGestionCases().getListeCases().get(i).getCelulle().getY() + 1) * (float)(largeurRect - 0.8), (float)(longueurRect - 0.8), (float)(largeurRect - 0.8));
		}
		shape.end();	

		// Tracé des lasers
		shape.begin(ShapeType.Filled);
		shape.setColor(Color.RED);
		for (int i = 0; i < terrain.getGestionTour().getListeTours().size(); i++)
		{
			if(terrain.getGestionTour().getListeTours().get(i).getMonstre() != null) {
				shape.rectLine((terrain.getGestionTour().getListeTours().get(i).getPosition().getX()), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() + (float)(largeurRect / 2)), terrain.getGestionTour().getListeTours().get(i).getMonstre().getPosition().getX(), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getMonstre().getPosition().getY() + (float)(largeurRect / 4)), 10);
			}
		}
		shape.end();

		// Tracé des monstres
		batch.begin();
		for(int i = 0; i < terrain.getGestionMonstre().getListeMonstres().size(); i++){
			batch.draw(monstre, (float)terrain.getGestionMonstre().getListeMonstres().get(i).getPosition().getX() - (monstre.getWidth()/2), (float)(Gdx.graphics.getHeight() - (terrain.getGestionMonstre().getListeMonstres().get(i).getPosition().getY() + 3)));
		}
		batch.end();

		// Tracé des tours
		batch.begin();
		for(int i = 0; i < terrain.getGestionTour().getListeTours().size(); i++){
			switch (terrain.getGestionTour().getListeTours().get(i).getType()) {
			case 1:
				batch.draw(tour1, (int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)), tour1.getWidth()/2, tour1.getHeight());
				break;
			case 2:
				batch.draw(tour2, (int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)), tour2.getWidth()/2, tour2.getHeight());
				break;
			case 3:
				batch.draw(tour3, (int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)), tour3.getWidth()/2, tour3.getHeight());
				break;
			default:
				break;
			}
		}
		batch.end();

		// Tracé des cercles autour des tours
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		shape.begin(ShapeType.Filled);
		{
			shape.setColor(new Color(0,0,0,0.2f));
			for(int i = 0; i < terrain.getGestionTour().getListeTours().size(); ++i)
			{
				shape.circle((int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2 + (float)(longueurRect - 0.8)/2),Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)/2), terrain.getGestionTour().getListeTours().get(i).getCollisionneur().getRayon());
			}
		}
		shape.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();

	}

	@Override
	public void dispose(){
		LibSong.dispose();
	}

	public void initialiserJeu() {

		longueurRect = (float)Gdx.graphics.getWidth() / 40;
		largeurRect = (float)Gdx.graphics.getHeight() / 40;
		camera = new PerspectiveCamera();
		longueurFenetre = Gdx.graphics.getWidth();
		largeurFenetre = Gdx.graphics.getHeight();
		terrain = new Terrain();
		terrain.initialiserTerrain((int)longueurFenetre, (int)largeurFenetre, (int)longueurRect, (int)largeurRect);

		reseauGUI = new ReseauGUI(terrain, isServer);
		reseauGUI.lancerJeu(isServer);
		
	}

	public void jouer() {

		Terrain.getTemps().setTempsPasse(Gdx.graphics.getDeltaTime() * 1000);
		entreesUtilisateur();
		terrain.jouer(true);

	}

	public void entreesUtilisateur() {

		Gdx.input.setInputProcessor
		(
				new InputAdapter()
				{
					public boolean touchDown(int x, int y, int pointer, int button)
					{
						if(button == Input.Buttons.LEFT)
						{
							for(int i = 0; i < terrain.getGestionCases().getListeCases().size(); i++) {
								if(terrain.getGestionCases().getListeCases().get(i).getPosition().getX()  < Gdx.input.getX() && (terrain.getGestionCases().getListeCases().get(i).getPosition().getX() + longueurRect) > Gdx.input.getX()) {
									if(terrain.getGestionCases().getListeCases().get(i).getPosition().getY() < Gdx.input.getY() && (terrain.getGestionCases().getListeCases().get(i).getPosition().getY() + largeurRect) > Gdx.input.getY()) {
										if(terrain.getGestionCases().getListeCases().get(i).isOccupe() == false) {
											x_tower = terrain.getGestionCases().getListeCases().get(i).getPosition().getX();
											y_tower = terrain.getGestionCases().getListeCases().get(i).getPosition().getY();
											terrain.getGestionTour().spawnTour(typeTour, new Position(x_tower + longueurRect/2, y_tower + largeurRect/2 - 4), terrain, i);
											LibSong.playSound("Son Pose");
											terrain.setaChange(true);
										}
									}
								}
							}
						}
						return true;
					} 
				}
				);

		boolean keyWPressed = Gdx.input.isKeyPressed(Keys.W); 
		if (keyWPressed)
		{
			typeTour = 1;
		}

		boolean keyXPressed = Gdx.input.isKeyPressed(Keys.X); 
		if (keyXPressed)
		{
			typeTour = 2;
		}

		boolean keyCPressed = Gdx.input.isKeyPressed(Keys.C); 
		if (keyCPressed)
		{
			typeTour = 3;
		}

	}

	//**********************************************************************************************************************	

//	//**************************************** Images Monstres ****************************************
//
//
//	Terrain terrain;
//	
//	float longueurFenetre, largeurFenetre, longueurRect, largeurRect, coefLongueur, coefLargeur, x_tower, y_tower, x_monster, y_monster;
//	private ShapeRenderer shape;
//	private SpriteBatch batch;
//	private Camera camera;
//	
//	Texture monstre1;
//	Texture tour1;
//	Texture tour2;
//	Texture tour3;
//	
//	int typeTour = 0;
//	
//
//	
//	@Override
//	public void create () {
//
//			shape = new ShapeRenderer();
//			
//			batch = new SpriteBatch();
//			monstre1 = new Texture(Gdx.files.internal("MonstreSquelette.png"));
//			tour1 = new Texture(Gdx.files.internal("TourBoisArcher.png"));
//			tour2 = new Texture(Gdx.files.internal("TourPierreJetPierre.png"));
//			tour3 = new Texture(Gdx.files.internal("TourPierreMortier.png"));
//			
//			initialiserJeu();
//		
//	}
//	
//	@Override
//	public void render() {
//		
//		jouer();
//		
//		Gdx.gl.glEnable(GL20.GL_BLEND);
//
//		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
//		Gdx.gl.glClearColor(1, 1, 1, 1);
//
//		Gdx.graphics.getGL20().glClearColor( 1, 1, 1, 1 );
//		Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
//
//		// Tracé des lignes
//		shape.begin(ShapeType.Line);
//		shape.setColor(Color.BLACK);
//		for(int i = 0; i < terrain.getGestionCases().getListeCases().size(); i++) {
//			shape.rect((terrain.getGestionCases().getListeCases().get(i).getCelulle().getX() + 1) * (float)(longueurRect - 0.8), (terrain.getGestionCases().getListeCases().get(i).getCelulle().getY() + 1) * (float)(largeurRect - 0.8), (float)(longueurRect - 0.8), (float)(largeurRect - 0.8));
//		}
//		shape.end();	
//
//		// Tracé des lasers
//		shape.begin(ShapeType.Filled);
//		shape.setColor(Color.RED);
//		
//		shape.end();
//		
//		// Tracé des monstres
//        batch.begin();
//		for(int i = 0; i < terrain.getGestionMonstre().getListeMonstres().size(); i++){
//			batch.draw(monstre1, (float)terrain.getGestionMonstre().getListeMonstres().get(i).getPosition().getX() - (monstre1.getWidth()/2), (float)(Gdx.graphics.getHeight() - (terrain.getGestionMonstre().getListeMonstres().get(i).getPosition().getY() + 3)));
//		}
//        batch.end();
//        
//        // Tracé des tours
//        batch.begin();
//		for(int i = 0; i < terrain.getGestionTour().getListeTours().size(); i++){
//			switch (terrain.getGestionTour().getListeTours().get(i).getType()) {
//			case 1:
//				batch.draw(tour1, (int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)), tour1.getWidth()/2, tour1.getHeight());
//				break;
//			case 2:
//				batch.draw(tour2, (int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)), tour2.getWidth()/2, tour2.getHeight());
//				break;
//			case 3:
//				batch.draw(tour3, (int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2), Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)), tour3.getWidth()/2, tour3.getHeight());
//				break;
//			default:
//				break;
//			}
//		}
//        batch.end();
//		
//		// Tracé des cercles autour des tours
//		Gdx.gl.glEnable(GL20.GL_BLEND);
//		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
//		shape.begin(ShapeType.Filled);
//		{
//			shape.setColor(new Color(0,0,0,0.2f));
//			for(int i = 0; i < terrain.getGestionTour().getListeTours().size(); ++i)
//			{
//				shape.circle((int)(terrain.getGestionTour().getListeTours().get(i).getPosition().getX() - longueurRect/2 + (float)(longueurRect - 0.8)/2),Gdx.graphics.getHeight() - (terrain.getGestionTour().getListeTours().get(i).getPosition().getY() - largeurRect/2 + 4 + (float)(largeurRect - 0.8)/2), terrain.getGestionTour().getListeTours().get(i).getCollisionneur().getRayon());
//			}
//		}
//		shape.end();
//		Gdx.gl.glDisable(GL20.GL_BLEND);
//		
//	}
//	
//	public void initialiserJeu() {
//		
//		longueurRect = (float)Gdx.graphics.getWidth() / 40;
//		largeurRect = (float)Gdx.graphics.getHeight() / 40;
//		camera = new PerspectiveCamera();
//		longueurFenetre = Gdx.graphics.getWidth();
//		largeurFenetre = Gdx.graphics.getHeight();
//	    
//	    terrain = new Terrain();
//	    terrain.initialiserTerrain((int)longueurFenetre, (int)largeurFenetre, (int)longueurRect, (int)largeurRect);
//	    
//		reseauGUI = new ReseauGUI(terrain, isServer);
//		reseauGUI.lancerJeu(isServer);
//	    
//	}
//	
//	public void jouer() {
//		
//		Terrain.getTemps().setTempsPasse(Gdx.graphics.getDeltaTime() * 1000);
//		entreesUtilisateur();
//		terrain.jouer(true);
//		
//	}
//	
//	public void entreesUtilisateur() {
//		
//		Gdx.input.setInputProcessor
//		(
//				new InputAdapter()
//				{
//					public boolean touchDown(int x, int y, int pointer, int button)
//					{
//						if(button == Input.Buttons.LEFT)
//						{
//							for(int i = 0; i < terrain.getGestionCases().getListeCases().size(); i++) {
//								if(terrain.getGestionCases().getListeCases().get(i).getPosition().getX()  < Gdx.input.getX() && (terrain.getGestionCases().getListeCases().get(i).getPosition().getX() + longueurRect) > Gdx.input.getX()) {
//									if(terrain.getGestionCases().getListeCases().get(i).getPosition().getY() < Gdx.input.getY() && (terrain.getGestionCases().getListeCases().get(i).getPosition().getY() + largeurRect) > Gdx.input.getY()) {
//										if(terrain.getGestionCases().getListeCases().get(i).isOccupe() == false) {
//											x_tower = terrain.getGestionCases().getListeCases().get(i).getPosition().getX();
//											y_tower = terrain.getGestionCases().getListeCases().get(i).getPosition().getY();
//											terrain.getGestionTour().spawnTour(typeTour, new Position(x_tower + longueurRect/2, y_tower + largeurRect/2 - 4), terrain, i);
//											terrain.getGestionCases().getListeCases().get(i).setOccupe(true);
//											terrain.getGestionCases().getListeCases().get(i).setMaTour(terrain.getGestionTour().getListeTours().get(terrain.getGestionTour().getListeTours().size() - 1));
//											terrain.setaChange(true);
//										}
//									}
//								}
//							}
//						}
//						return true;
//					} 
//				}
//			);
//		
//		boolean keyWPressed = Gdx.input.isKeyPressed(Keys.W); 
//		if (keyWPressed)
//		{
//			typeTour = 1;
//		}
//		
//		boolean keyXPressed = Gdx.input.isKeyPressed(Keys.X); 
//		if (keyXPressed)
//		{
//			typeTour = 2;
//		}
//		
//		boolean keyCPressed = Gdx.input.isKeyPressed(Keys.C); 
//		if (keyCPressed)
//		{
//			typeTour = 3;
//		}
//	
//	}
//
//	public Terrain getTerrain() {
//		return terrain;
//	}
//
//	public void setTerrain(Terrain terrain) {
//		this.terrain = terrain;
//	}
//		
////**********************************************************************************************************************	
	
}