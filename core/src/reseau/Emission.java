package reseau;

import java.io.PrintWriter;
import java.util.Scanner;


public class Emission implements Runnable {

	private PrintWriter out;
	private String message = null;
	private Scanner sc = null;
	private int id;
	
	public Emission(PrintWriter out, int id) {
		this.out = out;
		this.id = id;
	}
	
	public void run() {
		
		  sc = new Scanner(System.in);
		  
		  while(true){
			    System.out.println("Votre message :");
				message = sc.nextLine();
				out.println(id + "|" + message);
			    out.flush();
		  }
		  
	}
}
