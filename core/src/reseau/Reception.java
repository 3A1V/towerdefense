package reseau;

import java.io.BufferedReader;
import java.io.IOException;

public class Reception implements Runnable {

	private BufferedReader in;
	private String message = null;
	private int id = 0;
	
	public Reception(BufferedReader in, int id){
		this.in = in;
		this.id = id;
	}
	
	public void run() {
		
		while(true){
	        try {
			message = in.readLine();
			System.out.println("Message : " + message);
		    } catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

}
