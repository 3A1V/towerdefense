package serveur.reseau;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

import client.terrain.Terrain;
import affichage.JeuAD;
import affichage.JeuReseau;

public class Serveur {
	
	 private ServerSocket ss = null;
	 private Thread t;
	 private ArrayList<Socket> listeClients;
 
	 public Serveur() {
		 listeClients = new ArrayList<Socket>();
	 }

    public void Heberger() {
        
        try {
            ss = new ServerSocket(2010);
            System.out.println("Le serveur est à l'écoute du port " + ss.getLocalPort());
            
            t = new Thread(new NouvelleConnexion(ss, listeClients));
            t.start();
            
        } catch (IOException e) {
            System.err.println("Le port " + ss.getLocalPort() + " est déjà utilisé !");
        }
    
    }
    
}