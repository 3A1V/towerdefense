package serveur.reseau;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class NouvelleConnexion implements Runnable{

	private ServerSocket socketserver = null;
	private Socket socket = null;
	private Thread t2;
	private int nombresClients = 0;
	
	private ArrayList<Socket> listeClients;
	
	public NouvelleConnexion(ServerSocket ss, ArrayList<Socket> listeClients){
		socketserver = ss;
		this.listeClients = listeClients;
	}
	
	public void run() {
		
		try {
			while(true){
				socket = socketserver.accept();
				System.out.println("Un zéro veut se connecter : " + nombresClients);
				listeClients.add(socket);
				PrintWriter out = new PrintWriter(socket.getOutputStream());
				out.println("0" + ":" + (nombresClients + 1) + ":" + "1" + ":" + (nombresClients + 1));
			    out.flush();
			    
				t2 = new Thread(new ServeurEcoute(socket, listeClients));
				t2.start();
				nombresClients += 1;
			}
		} catch (IOException e) {
			System.err.println("Erreur serveur");
		}
		
	}
}
