package serveur.reseau;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import client.terrain.Terrain;

public class ServeurEcoute implements Runnable {

	private Socket socket = null;
	private BufferedReader in = null;
	private PrintWriter out = null;
	private ArrayList<Socket> listeClients;

	public ServeurEcoute(Socket s, ArrayList<Socket> listeClients) {
		socket = s;
		this.listeClients = listeClients;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));	
			while(true){
				String msg = in.readLine();
				int envoyeur = Integer.parseInt(msg.split(":")[0]);
				int destinataire = Integer.parseInt(msg.split(":")[1]);
				int idMsg = Integer.parseInt(msg.split(":")[2]);
				String contenu = (msg.split(":")[3]);
				switch (idMsg)  {
				case 0:
					break;
				case 1:
					break;
				case 2:	
					out = new PrintWriter(listeClients.get(destinataire - 1).getOutputStream());
					out.println(envoyeur + ":" + (destinataire - 1) + ":" + idMsg + ":" + contenu);
				    out.flush();
					break;
				}
				
			}
		} catch (IOException e) {	
			System.err.println("Le serveur ne répond plus ");
		}
		
	}
	
}
