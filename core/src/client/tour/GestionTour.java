package client.tour;

import java.util.ArrayList;
import client.physique.CollisionCercle;
import client.physique.Position;
import client.terrain.Terrain;

public class GestionTour {

	Position spawnPosition;
	ArrayList<Tour> listeTours;

	public GestionTour() {
		this.listeTours = new ArrayList<Tour>();
	}

	public void spawnTour(int typeTour, Position spawnPosition, Terrain terrain, int indexCase) {
		Tour maTour;
		boolean verif;

		switch (typeTour) {
		case 1:			
			maTour = new Tour(new CollisionCercle(30, spawnPosition), spawnPosition, 50, 50, 20, 1);
			break;
		case 2:
			maTour = new Tour(new CollisionCercle(30, spawnPosition), spawnPosition, 70, 70, 50, 2);
			break;
		case 3:
			maTour = new Tour(new CollisionCercle(30, spawnPosition), spawnPosition, 80, 80, 60, 3);
			break;
		default:
			maTour = new Tour(new CollisionCercle(30, spawnPosition), spawnPosition, 50, 50, 20, 1);
			break;
		}
		if(this.verifierArgentTour(maTour, terrain, typeTour)==true){
			verif = terrain.getGestionArgent().getMonArgent().verifierArgent(maTour.prix);
			if (verif == true) {
				terrain.getGestionArgent().getMonArgent().retirerArgent(maTour.getPrix());
				maTour.copierPosition(spawnPosition);
				listeTours.add(maTour);
				terrain.getGestionCases().getListeCases().get(indexCase).setOccupe(true);
				terrain.getGestionCases().getListeCases().get(indexCase).setMaTour(terrain.getGestionTour().getListeTours().get(terrain.getGestionTour().getListeTours().size() - 1));
				terrain.setaChange(true);
			}
			else{
				System.out.println("tour non crée");
			}
		}
	}


	public boolean verifierArgentTour(Tour tour, Terrain terrain, int typeTour){
		boolean Assezargent = false;
		if((typeTour==1) && (terrain.getGestionArgent().getMonArgent().verifierArgent(13))){
			Assezargent=true;
		}

		else if((typeTour==2) && (terrain.getGestionArgent().getMonArgent().verifierArgent(16))){
			Assezargent=true;
		}

		else if((typeTour==3) && (terrain.getGestionArgent().getMonArgent().verifierArgent(10))){
			Assezargent=true;
		}

		else if (terrain.getGestionArgent().getMonArgent().verifierArgent(10)){
			Assezargent=true;
		}

		return Assezargent;
	}


	public void actualiserEtatTours() {

		//		listeTours.removeIf
		//		(
		//			new Predicate<Tour>()
		//			{
		//				@Override
		//				public boolean test(Tour t) 
		//				{
		//					return !t.isVivant();
		//				}
		//				
		//			}
		//		);

		for(int i = 0; i < listeTours.size(); i++) {
			if (listeTours.get(i).getMonstre() != null) {
				if (!listeTours.get(i).getMonstre().isVivant()) {
					listeTours.get(i).setMonstre(null);
				}
			}

			if(!listeTours.get(i).isVivant()) {
				listeTours.remove(i);
				i = 0;
			}
		}

	}

	public Position getSpawnPosition() {
		return spawnPosition;
	}

	public void setSpawnPosition(Position spawnPosition) {
		this.spawnPosition = spawnPosition;
	}

	public ArrayList<Tour> getListeTours() {
		return listeTours;
	}

	public void setListeTours(ArrayList<Tour> listeTours) {
		this.listeTours = listeTours;
	}

}
