// Classe représentant une tour. Une tour appartient à une Case. 

package client.tour;

import client.monstre.Monstre;
import client.physique.CollisionCercle;
import client.physique.Position;

public class Tour {
	Position position;
	CollisionCercle collisionneur;
	int vie;
	boolean vivant;
	int attaque;
	int prix;
	Monstre cible;
	int type;

	public Tour(CollisionCercle collisionneur, Position position) {
		this.collisionneur = collisionneur;
		this.position = position;
		this.vivant = true;
	}

	public Tour(CollisionCercle collisionneur, Position position, int vie, int attaque) {
		this.collisionneur = collisionneur;
		this.position = position;
		this.vie = vie;
		this.attaque = attaque;
		this.vivant = true;
		this.prix = 10;
	}

	public Tour(CollisionCercle collisionneur, Position position, int vie, int attaque, int prix, int type) {
		this.collisionneur = collisionneur;
		this.position = position;
		this.vie = vie;
		this.attaque = attaque;
		this.vivant = true;
		this.prix = prix;
		this.type = type;
	}

	public Monstre getMonstre() {
		return cible;
	}

	public void setMonstre(Monstre cible) {
		this.cible = cible;
		this.prix = 20;
	}

	public CollisionCercle getCollisionneur() {
		return collisionneur;
	}

	public void setCollisionneur(CollisionCercle collisionneur) {
		this.collisionneur = collisionneur;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public int getAttaque() {
		return attaque;
	}

	public int getVie() {
		return vie;
	}

	public void setVie(int vie) {
		this.vie = vie;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public void retirerVie(int malusVie) {
		this.vie -= malusVie;
	}

	public void ajouterVie(int bonusVie) {
		this.vie += bonusVie;
	}

	public void vieZero() {
		if(this.vie <= 0) {
			this.vivant = false;
		}
	}

	public boolean isVivant() {
		return vivant;
	}

	public void setVivant(boolean vivant) {
		this.vivant = vivant;
	}

	public void copierPosition(Position position) {
		this.position.setX(position.getX());
		this.position.setY(position.getY());
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
