package client.monstre;

import java.util.ArrayList;

public class Vague {

	private String nom;
	private int nombreMonstres;
	private int delais;
	private ArrayList<Integer> listeMonstres;
	private int ordre;
	
	public Vague(String nom, int nombreMonstres, int delais, int ordre, ArrayList<Integer> listeMonstres) {
		this.nom = nom;
		this.delais = delais;
		this.nombreMonstres = nombreMonstres;
		this.listeMonstres = new ArrayList<Integer>();
		this.ordre = ordre;
		this.listeMonstres = listeMonstres;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNombreMonstres() {
		return nombreMonstres;
	}

	public void setNombreMonstres(int nombreMonstres) {
		this.nombreMonstres = nombreMonstres;
	}

	public int getDelais() {
		return delais;
	}

	public void setDelais(int delais) {
		this.delais = delais;
	}

	public ArrayList<Integer> getListeMonstres() {
		return listeMonstres;
	}

	public void setListeMonstres(ArrayList<Integer> listeMonstres) {
		this.listeMonstres = listeMonstres;
	}

	public int getOrdre() {
		return ordre;
	}

	public void setOrdre(int ordre) {
		this.ordre = ordre;
	}
	
}
