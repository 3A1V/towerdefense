// Classe représentant l'entité Monstre. En solo, il y a des monstres (ennemis uniquement), ils appartiennent au Terrain.

package client.monstre;

import java.util.ArrayList;
import client.physique.CollisionCercle;
import client.physique.Node;
import client.physique.Pathfinding;
import client.physique.Position;
import client.terrain.Case;
import client.terrain.Terrain;

public class Monstre {

	CollisionCercle collisionneur;
	Position position;
	Pathfinding pathfinding;
	Position cellule;
	ArrayList<Node> monChemin;
	boolean actualiserPath;
	int vie;
	boolean vivant;
	int attaque;
	int score;
	int prix;


	// Constructeur par défaut
	public Monstre(CollisionCercle collisionneur, Position position, Terrain terrain, int score) {
		this.collisionneur = collisionneur;
		this.position = position;
		ArrayList<Node> monChemin = new ArrayList<Node>();
		this.trouverCellule(terrain);
		this.actualiserPath = true;
		this.vivant = true;
		this.vie = 200;
		this.score = score;
		this.prix = 2;
	}
	
	public Monstre(CollisionCercle collisionneur, Position position, int vie, int attaque, Terrain terrain, int score) {
		this.collisionneur = collisionneur;
		this.position = position;
		ArrayList<Node> monChemin = new ArrayList<Node>();
		this.trouverCellule(terrain);
		this.actualiserPath = true;
		this.vivant = true;
		this.vie = vie;
		this.attaque = attaque;
		this.score = score;
		this.prix = 2;
	}
	
	public Monstre(CollisionCercle collisionneur, Position position, int vie, int attaque, Terrain terrain, int score, int prix) {
		this.collisionneur = collisionneur;
		this.position = position;
		ArrayList<Node> monChemin = new ArrayList<Node>();
		this.trouverCellule(terrain);
		this.actualiserPath = true;
		this.vivant = true;
		this.vie = vie;
		this.attaque = attaque;
		this.score = score;
		this.prix = prix;
	}
	
	// Procédure à effectuer pour les mouvements d'un monstre
	public void deplacer(Terrain terrain) {
	    this.trouverCellule(terrain);
	    
	    Position pos1 = new Position(this.cellule.getX(), this.cellule.getY());
	    Position pos2 = new Position(39, 38);
	    
	    if(!pos1.comparaison(pos2)) {
	    	if(actualiserPath || Terrain.isaChange()) {
	    		pathfinding = new Pathfinding(pos1, pos2, terrain.getGestionCases().getListeCases());
	    		monChemin = pathfinding.trouverChemin();
	    	}
	    	actualiserPath = false;
	    	if(monChemin != null) {
			    deplacement(monChemin.get(1), terrain);
			    if(monChemin.get(1).getX() == this.cellule.getX() && monChemin.get(1).getY() == this.cellule.getY()) {
			    	monChemin.remove(1);
			    }
	    	} else {
	    		System.out.println("Pas de chemin");
	    	}
	    } 

		this.collisionneur.deplacer(this.position);
	}
	
	// Deplacement du monstre à proprement parler, sa position
	void deplacement(Node noeud, Terrain terrain) {
		
		Position nouvellePosition = new Position(terrain.getGestionCases().getListeCases().get((noeud.getX() * 40) + (noeud.getY())).getPosition().getX() + Case.getLongueur()/2, terrain.getGestionCases().getListeCases().get((noeud.getX() * 40)+ (noeud.getY())).getPosition().getY() + Case.getLargeur()/2);
		
		if(Math.abs(this.position.getX() - nouvellePosition.getX()) > Math.abs(this.position.getY() - nouvellePosition.getY()) || Math.abs(this.position.getX() - nouvellePosition.getX()) >= 2) {
			if(this.position.getX() < nouvellePosition.getX()) {
				this.position.setX(this.position.getX() + 2);
			} else if(this.position.getX() > nouvellePosition.getX()) {
				this.position.setX(this.position.getX() - 2);
			}
		} else if(Math.abs(this.position.getX() - nouvellePosition.getX()) < Math.abs(this.position.getY() - nouvellePosition.getY()) && (Math.abs(this.position.getX() - nouvellePosition.getX()) <= 1)) {
			if(this.position.getY() < nouvellePosition.getY()) {
				this.position.setY(this.position.getY() + 2);
			} else if(this.position.getY() > nouvellePosition.getY()) {
				this.position.setY(this.position.getY() - 2);
			} 
		} else if(Math.abs(this.position.getX() - nouvellePosition.getX()) == Math.abs(this.position.getY() - nouvellePosition.getY())) {
			this.position.setX(nouvellePosition.getX());
			this.position.setY(nouvellePosition.getY());
		}
		
	}
	
	// Permet de déterminer sur quelle cellule se trouve le monstre
	public void trouverCellule(Terrain terrain) {
		this.cellule = new Position(0, 0);
		for(int i = 0; i < terrain.getGestionCases().getListeCases().size(); i++) {
			if(terrain.getGestionCases().getListeCases().get(i).getPosition().getX()  < position.getX() && (terrain.getGestionCases().getListeCases().get(i).getPosition().getX() + Case.getLongueur()) > position.getX()) {
				if(terrain.getGestionCases().getListeCases().get(i).getPosition().getY() < position.getY() && (terrain.getGestionCases().getListeCases().get(i).getPosition().getY() + Case.getLargeur()) > position.getY()) {
					this.cellule = terrain.getGestionCases().getListeCases().get(i).getCelulle();
					break;
				} else if(terrain.getGestionCases().getListeCases().get(i).getPosition().getY() == position.getY()) {
					this.cellule = terrain.getGestionCases().getListeCases().get(i).getCelulle();
					break;
				}
			} else if(terrain.getGestionCases().getListeCases().get(i).getPosition().getX() == position.getX()) {
				if(terrain.getGestionCases().getListeCases().get(i).getPosition().getY() < position.getY() && (terrain.getGestionCases().getListeCases().get(i).getPosition().getY() + Case.getLargeur()) > position.getY()) {
					this.cellule = terrain.getGestionCases().getListeCases().get(i).getCelulle();
					break;
				} else if(terrain.getGestionCases().getListeCases().get(i).getPosition().getY() == position.getY()) {
					this.cellule = terrain.getGestionCases().getListeCases().get(i).getCelulle();
					break;
				}
			}
		}
	}

	public CollisionCercle getCollisionneur() {
		return collisionneur;
	}

	public void setCollisionneur(CollisionCercle collisionneur) {
		this.collisionneur = collisionneur;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
	public int getVie() {
		return vie;
	}

	public void setVie(int vie) {
		this.vie = vie;
	}
	
	public void retirerVie(int malusVie) {
		this.vie -= malusVie;
	}
	
	public void ajouterVie(int bonusVie) {
		this.vie += bonusVie;
	}
	
	public void vieZero() {
		if(this.vie <= 0) {
			this.vivant = false;
		}
	}

	public boolean isVivant() {
		return vivant;
	}

	public void setVivant(boolean vivant) {
		this.vivant = vivant;
	}
	
	public int getAttaque() {
		return attaque;
	}
	
	public int getScore() {
		return score;
	}
	
	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}
	
	public void copierPosition(Position position) {
		this.position.setX(position.getX());
		this.position.setY(position.getY());
	}
	
}
