package client.monstre;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Predicate;

import client.physique.CollisionCercle;
import client.physique.Position;
import client.terrain.Terrain;

public class GestionMonstre {

	Timer timer;

	Position spawnPosition;
	boolean peutSpawnNouveauMonstre;
	ArrayList<Monstre> listeMonstres;

	public GestionMonstre(Position position) {
		this.spawnPosition = position;
		this.peutSpawnNouveauMonstre = true;
		this.listeMonstres = new ArrayList<Monstre>();
	}

	public void spawnMonstre(int typeMonstre, Terrain terrain) {
		Monstre monMonstre;
		switch (typeMonstre) {
		case 1:
			monMonstre = new Monstre(new CollisionCercle(10, new Position(0, 0)), new Position(0, 0), 500, 3, terrain, 1, 17);
			break;
		case 2:
			monMonstre = new Monstre(new CollisionCercle(10, new Position(0, 0)), new Position(0, 0), 100, 5, terrain, 2, 2);
			break;
		case 3:
			monMonstre = new Monstre(new CollisionCercle(10, new Position(0, 0)), new Position(0, 0), 300, 7, terrain, 3, 15);
			break;
		case 4:
			monMonstre = new Monstre(new CollisionCercle(10, new Position(0, 0)), new Position(0, 0), 1500, 10, terrain, 3, 999999);
			break;
		default:
			monMonstre = new Monstre(new CollisionCercle(10, new Position(0, 0)), new Position(0, 0), 300, 1, terrain, 4, 1);
			break;
		}


		monMonstre.copierPosition(spawnPosition);
		getListeMonstres().add(monMonstre);
	}

	public void spawnVague(ArrayList<Integer> listeMonstres, Terrain terrain, int delais) {
		if(peutSpawnNouveauMonstre && listeMonstres.size() > 0) {
			spawnMonstre(listeMonstres.get(0), terrain);
			peutSpawnNouveauMonstre = false;
			listeMonstres.remove(0);
			BeepSpawn(delais);

		}
	}

	public void verifierNombreMonstres(Terrain terrain) {
		if(getListeMonstres().size() <= 0 && terrain.getGestionVagues().getVagueActuelle().getListeMonstres().size() == 0){
			if(terrain.getGestionVagues().isVagueEnCours()) {
				terrain.getGestionVagues().vagueTerminee();
				terrain.getGestionVagues().setVagueEnCours(false);
			}
		}
	}

	public void actualiserEtatMonstres(Terrain terrain) {

		for(int i = 0; i < listeMonstres.size(); i++) {
			if(!listeMonstres.get(i).isVivant()) {
				terrain.getGestionScore().getMonScore().ajouterScore((listeMonstres.get(i).getScore()));
				terrain.getGestionVagues().getVagueActuelle().setNombreMonstres(terrain.getGestionVagues().getVagueActuelle().getNombreMonstres()-1);
				terrain.getGestionArgent().getMonArgent().ajouterArgent(terrain.getGestionMonstre().getListeMonstres().get(i).getPrix());
				listeMonstres.remove(i);
				i = 0;
			}
		}

	}

	// Permet d'effectuer les actions liées au déplacement des monstres
	public void deplacerMonstres(Terrain terrain) {
		for(int i = 0; i < getListeMonstres().size(); i++) {
			getListeMonstres().get(i).deplacer(terrain);
		}
		terrain.setaChange(false);
	}

	public ArrayList<Monstre> getListeMonstres() {
		return listeMonstres;
	}

	// Fonctionnalité permettant de faire un timer
	public void BeepSpawn(int time) {
		timer = new Timer();
		timer.schedule(new TimerSpawn(), time);
	}
	class TimerSpawn extends TimerTask {
		public void run() {
			peutSpawnNouveauMonstre = true;
			timer.cancel();
		}
	}

}
