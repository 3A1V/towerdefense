package client.monstre;

import java.util.ArrayList;

import client.terrain.Terrain;

public class GestionVagues {

	private Vague vagueActuelle;
	private ArrayList<Vague> listeVagues;
	private boolean vagueEnCours;

	public GestionVagues() {
		this.listeVagues = new ArrayList<Vague>();
		this.vagueEnCours = false;
	}

	public void actualiserVague(Terrain terrain) {
		if(vagueEnCours == false && listeVagues.size() != 0) {
			amorcerVague(terrain);
		} else if(vagueEnCours) {
			jouerVague(vagueActuelle, terrain);
		}
	}

	public void preparerVagues() {
		ArrayList<Integer> listeMonstres = new ArrayList<Integer>();
		for(int i = 0; i < 20; i++) {
			listeMonstres.add(2);
		}
		ajouterVague(new Vague("Première Vague", listeMonstres.size(), 500, 1, listeMonstres));

		listeMonstres = new ArrayList<Integer>();
		for(int i = 0; i < 10; i++) {
			listeMonstres.add(3);
		}
		ajouterVague(new Vague("Deuxième Vague", listeMonstres.size(), 2000, 2, listeMonstres));
		
		listeMonstres = new ArrayList<Integer>();
		for(int i = 0; i < 10; i++) {
			listeMonstres.add(1);
		}
		ajouterVague(new Vague("Troisième Vague", listeMonstres.size(), 2000, 3, listeMonstres));
		
		listeMonstres = new ArrayList<Integer>();
		for(int i = 0; i < 1; i++) {
			listeMonstres.add(4);
		}
		ajouterVague(new Vague("Astaröth", listeMonstres.size(), 2000, 4, listeMonstres));
	}

	public void amorcerVague(Terrain terrain) {
		int index = 0;
		int ordre = 100;
		for(int i = 0; i < listeVagues.size(); i++) {
			if(ordre > listeVagues.get(i).getOrdre()) {
				ordre = listeVagues.get(i).getOrdre();
				index = i;
			}
		}
		vagueActuelle = listeVagues.get(index);
		System.out.println(vagueActuelle + " : " + vagueActuelle.getNom());
		vagueEnCours = true;
		actualiserVague(terrain);
	}

	public void vagueTerminee() {
		listeVagues.remove(vagueActuelle);
	}

	public void jouerVague(Vague vague, Terrain terrain) {
		terrain.getGestionMonstre().spawnVague(vague.getListeMonstres(), terrain, vague.getDelais());
	}

	public ArrayList<Vague> getListeVagues() {
		return listeVagues;
	}

	public void setListeVagues(ArrayList<Vague> listeVagues) {
		this.listeVagues = listeVagues;
	}

	public void ajouterVague(Vague vague) {
		this.listeVagues.add(vague);
	}

	public boolean isVagueEnCours() {
		return vagueEnCours;
	}

	public void setVagueEnCours(boolean vagueEnCours) {
		this.vagueEnCours = vagueEnCours;
	}

	public Vague getVagueActuelle() {
		return vagueActuelle;
	}

	public void setVagueActuelle(Vague vagueActuelle) {
		this.vagueActuelle = vagueActuelle;
	}

}
