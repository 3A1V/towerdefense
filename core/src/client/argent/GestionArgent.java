package client.argent;

import client.tour.Tour;

public class GestionArgent {

	Argent monArgent;

	public GestionArgent(Argent monArgent) {
		this.monArgent = monArgent;
	}

	public Argent getMonArgent() {
		return monArgent;
	}

	public void setMonArgent(Argent monArgent) {
		this.monArgent = monArgent;
	}
	
	
	
}