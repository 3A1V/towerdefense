package client.argent;

public class Argent {

	int solde;
	boolean plusDargent;

	public Argent(int solde) {
		if(solde>0) {
			this.plusDargent = false; 
		}
		else {
			this.plusDargent = true;
		}
		this.solde = solde;
	}
	
	public boolean verifierArgent(int valeur) {
		if(this.solde - valeur >= 0) {
			return true;
		}
		else {
			return false;
		}
	}

	public int getSolde() {
		return solde;
	}

	public void setSolde(int solde) {
		this.solde = solde;
	}

	public void retirerArgent(int sommePayee) {
		if(this.solde-sommePayee >= 0) {
			this.solde -= sommePayee;
		}
	}

	public void ajouterArgent(int solde) {
		this.solde += solde;
	}

	public boolean getPlusDargent() {
		return plusDargent;
	}

	public void setPlusDargent(boolean plusDargent) {
		this.plusDargent = plusDargent;
	}
}
