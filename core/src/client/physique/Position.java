// Classe utilisée par toutes les entités instanciées. Elle permet de déterminer une position X et Y.
// Elle peut être utilisée pour la position d'une cellule ou bien d'un pixel, par exemple

package client.physique;

public class Position {

	float x;
	float y;
	
	public Position(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	// Permet d'afficher une position plus facilement
	@Override public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(" X : " + x + " et Y : " + y);
		return result.toString();
	}
	
	// Permet de comparer deux positions et d'indiquer si elles sont identiques
	public boolean comparaison(Position position) {
		if(this.x == position.getX() && this.y == position.getY()) {
			return true;
		}
		return false;
	}

}
