// Classe utilisée pour calculer un chemin. Le pathfinding utilisé est AStar.
// Trop utilisée, cette classe peut couter cher en ressources.

package client.physique;

import java.util.ArrayList;
import java.util.Collections;

import client.terrain.Case;

public class Pathfinding {
	
	ArrayList<Node> listeNodesOuverts;
	ArrayList<Node> listeNodesFermes;
	ArrayList<Node> listeNodes;
	Node nodeActuel;
	Node nodeDepart;
	Node nodeArrivee;
	Node nodeTest;
	ArrayList<Node> listeNodesConnectes;
	int coutTrajet;
	
	// Constructeur par défaut
	public Pathfinding(Position celluleDepart, Position celluleArrivee, ArrayList<Case> listeCases) {
		listeNodesOuverts = new ArrayList<Node>();
		listeNodesFermes = new ArrayList<Node>();
		listeNodesConnectes = new ArrayList<Node>();
		listeNodes = preparerNodes(listeCases);
		nodeActuel = new Node(celluleDepart, true);
		nodeDepart = new Node(celluleDepart, true);
		nodeArrivee = new Node(celluleArrivee, true);
		attribuerNodes();
		coutTrajet = 1;
		nodeActuel.setG(0);
	}
	
	// Permet de convertir les cases (changements dynamiques) en Node
	public ArrayList<Node> preparerNodes(ArrayList<Case>listeCases) {
		ArrayList<Node> listeNodesConvertis = new ArrayList<Node>();
		for(int i = 0; i < listeCases.size(); i++) {
			listeNodesConvertis.add(new Node(listeCases.get(i).getCelulle(), !listeCases.get(i).isOccupe()));
		}
		return listeNodesConvertis;
	}
	
	// Attribue les nodes de départ et d'arrivée
	public void attribuerNodes() {
		for(int i = 0; i < listeNodes.size(); i++) {
			if(nodeDepart.getX() == listeNodes.get(i).getX() && nodeDepart.getY() == listeNodes.get(i).getY()) {
				nodeDepart = listeNodes.get(i);
			} else if(nodeArrivee.getX() == listeNodes.get(i).getX() && nodeArrivee.getY() == listeNodes.get(i).getY()) {
				nodeArrivee = listeNodes.get(i);
			}
		}
	}
	
	// Algorithme de base AStar
	public ArrayList<Node> trouverChemin() {
		
		int g;
		int h;
		int f;
		
		while (nodeActuel != nodeArrivee) {
			listeNodesConnectes = trouverVoisins();
			for (int i = 0; i < listeNodesConnectes.size(); ++i) {
				nodeTest = listeNodesConnectes.get(i);
				if (nodeTest == nodeActuel || nodeTest.isTraversable() == false) continue;
				g = nodeActuel.g  + coutTrajet;
				h = manhattan();
				f = g + h;
				if ( estOuvert() || estFerme())	{
					if(nodeTest.f > f)
					{

						nodeTest.f = f;
						nodeTest.g = g;
						nodeTest.h = h;
						nodeTest.parentNode = nodeActuel;
					}
				}else {
					nodeTest.f = f;
					nodeTest.g = g;
					nodeTest.h = h;
					nodeTest.parentNode = nodeActuel;
					listeNodesOuverts.add(nodeTest);
				}
			}
			listeNodesFermes.add(nodeActuel);
			if (listeNodesOuverts.size() == 0) {
				return null;
			}
			nodeActuel = trouverPetitF();
			listeNodesOuverts.remove(nodeActuel);
			//listeNodesOuverts.sortOn('f', Array.NUMERIC);
			//nodeActuel = listeNodesOuverts.shift() as INode;
		}
		return construireChemin();
	}
	
	// Permet de retourner le plus petit F des nodes
	public Node trouverPetitF() {
		Node premierNode = null;
		if(listeNodesOuverts.size() != 0) {
			premierNode = listeNodesOuverts.get(0);
			for(int i = 0; i < listeNodesOuverts.size(); i++) {
				if(listeNodesOuverts.get(i).getF() < premierNode.getF()) {
					premierNode = listeNodesOuverts.get(i);
				}
			}
		}
		return premierNode;
	}
	
	// Indique si oui ou non un Node est "ouvert"
	public boolean estOuvert() {
		for(int i = 0; i < listeNodesOuverts.size(); i++) {
			if(listeNodesOuverts.get(i) == nodeTest) {
				return true;
			}
		}
		return false;
	}
	
	// Indique si oui ou non un Node est "fermé"
	public boolean estFerme() {
		for(int i = 0; i < listeNodesFermes.size(); i++) {
			if(listeNodesFermes.get(i) == nodeTest) {
				return true;
			}
		}
		return false;
	}
	
	// Permet de déterminer les voisins du Node actuel
	public ArrayList<Node> trouverVoisins() {
		ArrayList<Node> nodesVoisins = new ArrayList<Node>();
		
		for(int i = 0; i < listeNodes.size(); i++) {
			if((listeNodes.get(i).getX() == nodeActuel.getX() - 1) && (listeNodes.get(i).getY() == nodeActuel.getY())) {
				nodesVoisins.add(listeNodes.get(i));
			} else if((listeNodes.get(i).getX() == nodeActuel.getX() + 1) && (listeNodes.get(i).getY() == nodeActuel.getY())) {
				nodesVoisins.add(listeNodes.get(i));
			} else if((listeNodes.get(i).getX() == nodeActuel.getX()) && (listeNodes.get(i).getY() == nodeActuel.getY() - 1)) {
				nodesVoisins.add(listeNodes.get(i));
			} else if((listeNodes.get(i).getX() == nodeActuel.getX()) && (listeNodes.get(i).getY() == nodeActuel.getY() + 1)) {
				nodesVoisins.add(listeNodes.get(i));
			}
		}
		
		return nodesVoisins;
	}
	
	// Algorithme qui calcule la distance entre deux cellules (ici sans diagonales)
	public int manhattan() {
		return Math.abs(nodeTest.x - nodeArrivee.x) * coutTrajet + Math.abs(nodeTest.y + nodeArrivee.y) * coutTrajet;
	}
	
	// Permet de construire le chemin après être arrivé à la cellule finale dans l'algorithme principal
	public ArrayList<Node> construireChemin() {
		ArrayList<Node> cheminConstruit = new ArrayList<Node>();
		Node node = nodeArrivee;
		cheminConstruit.add(node);
		boolean isNodeDepart = false;
		while (!isNodeDepart) {
			node = node.parentNode;
			cheminConstruit.add(node);
			isNodeDepart = (node.getX() == nodeDepart.getX() & node.getY() == nodeDepart.getY());
		}
		Collections.reverse(cheminConstruit);
		return cheminConstruit;
	}
	
}
