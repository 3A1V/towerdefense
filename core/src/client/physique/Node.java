// Classe utilisée dans le pathfinding AStar. Représente grosso modo une Case adaptée pour le AStar.

package client.physique;

public class Node {

	int f;
	int g;
	int h;
	int x;
	int y;
	Node parentNode;
	boolean traversable;
	
	public Node(Position cellule, boolean traversable) {
		
		this.x = (int)cellule.getX();
		this.y = (int)cellule.getY();
		this.traversable = traversable;
		
	}
	
	public int getF() {
		return f;
	}
	
	public void setF(int f) {
		this.f = f;
	}
	
	public int getG() {
		return g;
	}
	
	public void setG(int g) {
		this.g = g;
	}
	
	public int getH() {
		return h;
	}
	
	public void setH(int h) {
		this.h = h;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public Node getParentNode() {
		return parentNode;
	}
	
	public void setParentNode(Node parentNode) {
		this.parentNode = parentNode;
	}
	
	public boolean isTraversable() {
		return traversable;
	}
	
	public void setTraversable(boolean traversable) {
		this.traversable = traversable;
	}
	
}
