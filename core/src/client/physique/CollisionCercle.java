// Classe représentant l'entité d'un Collisionneur en forme de cercle. Chaque objet devant donc détecter des collisions
// en cercle pourra se voir doter de cette classe. (Tour par exempe)

package client.physique;

public class CollisionCercle {

	int rayon;
	Position position;

	public CollisionCercle(int rayon, Position position) {
		this.rayon = rayon;
		this.position = position;
	}
	
	// Permet de suivre l'objet sur lequel est attachée le collisionneur
	public void deplacer(Position nouvellePosition) {
		this.position = nouvellePosition;
	}
	
	// Permet de déterminer si oui ou non l'objet passé en paramètre est en collision avec l'objet détenant le collisionneur
	public boolean detectionCollision(Position positionCollision, int rayon) {
		
		float carreX = (float)(Math.pow(positionCollision.getX() - this.position.getX(), 2));
		float carreY = (float)(Math.pow(positionCollision.getY() - this.position.getY(), 2));
		
		float distanceCarre = (float)(Math.sqrt(carreX + carreY));
		
		if(distanceCarre <= this.rayon/2 + rayon && distanceCarre >= 0) {
			return true;
		}
		return false;
	}

	public int getRayon() {
		return rayon;
	}

	public void setRayon(int rayon) {
		this.rayon = rayon;
	}

}
