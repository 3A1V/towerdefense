package client.physique;

import client.monstre.Monstre;
import client.terrain.Terrain;
import client.tour.Tour;

public class GestionCollision {

	public GestionCollision() {

	}

	public void actualiserCollisions(Terrain terrain) {

		for (Monstre monstre :  terrain.getGestionMonstre().getListeMonstres())
		{
			if(monstre.getCollisionneur().detectionCollision(terrain.getChateau().getPosition(), terrain.getChateau().getCollisionneur().getRayon())) {
				terrain.getChateau().retirerVie(monstre.getAttaque());
				terrain.getChateau().verifierVie();
			}
		}

		for (Tour tour :  terrain.getGestionTour().getListeTours())
		{
			for (Monstre monstre :  terrain.getGestionMonstre().getListeMonstres())
			{

				if(monstre.getCollisionneur().detectionCollision(tour.getPosition(), tour.getCollisionneur().getRayon()))
				{

					tour.retirerVie(monstre.getAttaque());
					tour.vieZero();

					if (tour.getMonstre() == null) {
						tour.setMonstre(monstre);
					} 

					if (tour.getMonstre() == monstre) {
						tour.getMonstre().retirerVie(tour.getAttaque());
						tour.getMonstre().vieZero();
					}

				} else {
					if (tour.getMonstre() != null && tour.getMonstre() == monstre) {
						tour.setMonstre(null);
					} 
				}
			}
		}
	}
}
