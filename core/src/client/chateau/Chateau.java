// Classe représentant l'entité Château, en solo il y en a donc un. Un château appartient à une Case.

package client.chateau;

import client.physique.CollisionCercle;
import client.physique.Position;

public class Chateau {

	Position position;
	CollisionCercle collisionneur;
	int vie;
	boolean vivant;

	public Chateau(int vie, Position position) {
		this.vie = vie;
		this.position = position;
		this.collisionneur = new CollisionCercle(30, position);
		this.vivant = true;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public CollisionCercle getCollisionneur() {
		return collisionneur;
	}

	public void setCollisionneur(CollisionCercle collisionneur) {
		this.collisionneur = collisionneur;
	}

	public boolean isVivant() {
		return vivant;
	}

	public void setVivant(boolean vivant) {
		this.vivant = vivant;
	}

	public int getVie() {
		return vie;
	}

	public void setVie(int vie) {
		this.vie = vie;
	}

	public void retirerVie(int malusVie) {
		if(verifRetirerVie(malusVie)) {
			this.vie -= malusVie;
		}
	}

	public void ajouterVie(int bonusVie) {
		this.vie += bonusVie;
	}

	public void verifierVie() {
		if(this.getVie() <= 0) {
			this.vivant = false;
		}
	}

	public boolean verifRetirerVie(int vieRetiree) {
		if (this.vie - vieRetiree < 0) {
			this.vie = 0;
			return false;
		}
		else {
			return true;
		}
	}

}
