package client.reseau;

import java.io.*;
import java.net.*;
import java.util.Scanner;
import reseau.Emission;
import reseau.Reception;


public class DonneesReseauClient implements Runnable {

	private Socket socket;
	private PrintWriter out = null;
	private BufferedReader in = null;
	private Scanner sc;
	private Thread t3, t4;
	private int id;
	
	public DonneesReseauClient(Socket s, int id){
		socket = s;
		this.id = id;
	}
	
	public void run() {
		try {
			out = new PrintWriter(socket.getOutputStream());
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			sc = new Scanner(System.in);
			Thread t4 = new Thread(new Emission(out, id));
			t4.start();
			Thread t3 = new Thread(new Reception(in, 0));
			t3.start();
		} catch (IOException e) {
			System.err.println("Le serveur distant s'est déconnecté !");
		}
	}

}
