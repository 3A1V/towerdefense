package client.reseau;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import client.terrain.Terrain;

import com.badlogic.gdx.ApplicationListener;

import affichage.JeuAD;
import affichage.JeuReseau;
import serveur.reseau.Serveur;

public class ReseauGUI extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;
	private JLabel label;
	private JTextField tf;
	private JButton envoyer;
	
	Client client;
	Serveur serveur;
	
	public ReseauGUI(Terrain jeu, boolean isServer) {
		
		if(isServer) {
			serveur = new Serveur();
		}
		client = new Client(jeu);
		
		JPanel PanelNord = new JPanel(new GridLayout(3,1));
		PanelNord.setBackground(Color.white);
		
		JPanel PortEtServeur = new JPanel(new GridLayout(1,5, 1, 3));
		PortEtServeur.setBackground(Color.white);

		PortEtServeur.add(new JLabel(""));
		PanelNord.add(PortEtServeur);

		label = new JLabel("Entrez le type de monstre à envoyer", SwingConstants.CENTER);
		PanelNord.add(label);
		tf = new JTextField("");
		tf.setBackground(Color.WHITE);
		PanelNord.add(tf);
		add(PanelNord, BorderLayout.NORTH);

		envoyer = new JButton("Envoyer monstre");
		envoyer.setBackground(Color.white);
		envoyer.addActionListener(this);

		JPanel southPanel = new JPanel();
		southPanel.add(envoyer);
		add(southPanel, BorderLayout.SOUTH);

		setSize(300, 150);
		setLocation(980, 0);
		setVisible(true);
		tf.requestFocus();

	}
	
	public void lancerJeu(boolean isServer) {
		if(isServer == true) {
			serveur.Heberger();
			client.Connecter();
		} else {
			client.Connecter();
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object o = arg0.getSource();
		
		if(o == envoyer) {
			client.EnvoyerMonstre();
		}
		
	}

}
