package client.reseau;

import java.net.*;
import java.util.Scanner;
import java.io.*;

public class Connexion implements Runnable {

	private Socket socket = null;
	public Thread t2;
	private PrintWriter out = null;
	private BufferedReader in = null;
	private Scanner sc = null;
	private boolean connect = false;
	private int id = 0;
	
	public Connexion(Socket s){
		
		socket = s;
	}
	
	public void run() {
		
		try {
			
			out = new PrintWriter(socket.getOutputStream());
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));	
			sc = new Scanner(System.in);
		
			while(!connect){
				
				String msgConnexion = in.readLine();
				
				if(msgConnexion.contains("connecte")){
					System.out.println("Je suis connecté"); 
					id = Integer.parseInt(msgConnexion.split("|")[0]);
					connect = true;
				} else {
					System.err.println("Connexion refusée"); 
				}
			
			}
				
			t2 = new Thread(new DonneesReseauClient(socket, id));
			t2.start();
		
		} catch (IOException e) {	
			System.err.println("Le serveur ne répond plus ");
		}
	}

}
