package client.reseau;

import java.io.*;
import java.net.*;

import client.terrain.Terrain;

public class Client {

    private Socket socket = null;
    private Thread t1;
	private PrintWriter out = null;
    
    private Terrain jeu;
    
    public Client(Terrain jeu) {
    	this.jeu = jeu;
    }
    
   	public void Connecter() {
   		
	    try {
	        
	        System.out.println("Demande de connexion");
	        socket = new Socket("127.0.0.1", 2010);
	        System.out.println("Connexion établie avec le serveur");
	        
	        t1 = new Thread(new ClientEcoute(socket, jeu));
	        t1.start();
  
	    } catch (UnknownHostException e) {
	    	System.err.println("Impossible de se connecter à l'adresse " + socket.getLocalAddress());
	    } catch (IOException e) {
	    	System.err.println("Aucun serveur à l'écoute du port " + socket.getLocalPort());
	    }
	    
    }
   	
   	public void EnvoyerMonstre() {
   		
		try {
			out = new PrintWriter(socket.getOutputStream());
			out.println(jeu.getId() + ":" + "2" + ":" + "2" + ":" + "1");
		    out.flush();
		} catch (IOException e) {	
			System.err.println("Le serveur ne répond plus ");
		}
   		
   	}

}