package client.reseau;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

import client.terrain.Terrain;

public class ClientEcoute implements Runnable {

	private Socket socket = null;
	private BufferedReader in = null;
	private Terrain jeu;
	
	
	public ClientEcoute(Socket s, Terrain jeu) {
		socket = s;
		this.jeu = jeu;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));	
			while(true){
				String msg = in.readLine();
				int envoyeur = Integer.parseInt(msg.split(":")[0]);
				int destinataire = Integer.parseInt(msg.split(":")[1]);
				int idMsg = Integer.parseInt(msg.split(":")[2]);
				String contenu = (msg.split(":")[3]);
				switch (idMsg)  {
				case 0:
					break;
				case 1:
					jeu.setId(Integer.parseInt(contenu));
					System.out.println("Ton id est : " + jeu.getId());
					break;
				case 2:
					jeu.getGestionMonstre().spawnMonstre(Integer.parseInt(contenu), jeu);
					break;
				}
				
			}
		} catch (IOException e) {	
			System.err.println("Le serveur ne répond plus ");
		}
	}

}
