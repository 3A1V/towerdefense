// Classe permettant de simuler des cases sur le terrain. Les cases appartiennent au terrain. Elles sont dans une liste.

package client.terrain;

import client.chateau.Chateau;
import client.physique.Position;
import client.tour.Tour;

public class Case {
	static int longueur;
	static int largeur;
	Position celulle;
	Position position;
	boolean occupe;
	Tour maTour;
	Chateau monChateau;

	public Case (Position position) {
		this.position = position;
	}
	
	public Case (Position position, boolean occupe) {
		this.position = position;
		this.occupe = occupe;
	}
	
	public Case(Position celulle, Position position) {
		this.celulle = celulle;
		this.position = position;
	}
	
	public Case(Position celulle, Position position, boolean occupe) {
		this.celulle = celulle;
		this.position = position;
		this.occupe = occupe;
	}
	
	public Case (Position position, boolean occupe, Tour maTour) {
		this.position = position;
		this.occupe = occupe;
		this.maTour = maTour;
	}
	
	public Case(Position celulle, Position position, boolean occupe, Chateau monChateau) {
		this.celulle = celulle;
		this.position = position;
		this.occupe = occupe;
		this.monChateau = monChateau;
	}
	
	public Position getPosition() {
		return position;
	}
	
	public void setPosition(Position position) {
		this.position = position;
	}

	public boolean isOccupe() {
		return occupe;
	}

	public void setOccupe(boolean occupe) {
		this.occupe = occupe;
	}

	public Chateau getMonChateau() {
		return monChateau;
	}

	public void setMonChateau(Chateau monChateau) {
		this.monChateau = monChateau;
	}

	public Position getCelulle() {
		return celulle;
	}

	public void setCelulle(Position celulle) {
		this.celulle = celulle;
	}

	public static int getLongueur() {
		return longueur;
	}

	public static void setLongueur(int longueur) {
		Case.longueur = longueur;
	}

	public static int getLargeur() {
		return largeur;
	}

	public static void setLargeur(int largeur) {
		Case.largeur = largeur;
	}

	public Tour getMaTour() {
		return maTour;
	}

	public void setMaTour(Tour maTour) {
		this.maTour = maTour;
	}
	
}