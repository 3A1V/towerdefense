package client.terrain;

import java.util.ArrayList;

import client.physique.Position;

public class GestionCases {

	private ArrayList<Case> listeCases;
	
	public GestionCases() {
		this.listeCases = new ArrayList<Case>();
	}
	
	public void actualiserEtatCases(Terrain terrain) {
		for(int i = 0; i < listeCases.size(); i++) {
			if(listeCases.get(i).getMaTour() != null) {
				if(listeCases.get(i).getMaTour().isVivant() == false && listeCases.get(i).isOccupe()) {
					listeCases.get(i).setOccupe(false);
					listeCases.get(i).setMaTour(null);
					System.out.println("Libération d'une case");
					terrain.setaChange(true);
				}
			}
		}
	}
	
	// Permet d'initialiser les cases en leur attribuant une position..
	public void initialiserCases(int longueurFenetre, int largeurFenetre, int longueurRect, int largeurRect) {
		Case.setLongueur(longueurRect);
		Case.setLargeur(largeurRect);
		for(int x = 0; x < Terrain.tailleTerrain; x++) {
			for(int y = 0; y < Terrain.tailleTerrain; y++) {
				listeCases.add(new Case(new Position(x, y), new Position((int)((x + 1) * (float)(longueurRect - 0.8)), (int)((y + 2) * (float)(largeurRect - 0.8))), false));
			}
		}
	}
	
	public ArrayList<Case> getListeCases() {
		return listeCases;
	}

	public void setListeCases(ArrayList<Case> listeCases) {
		this.listeCases = listeCases;
	}
	
	
}
