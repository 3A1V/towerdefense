// Classe très importante. Elle contient entre autres une liste de Cases, static (accessible par toutes les autres classes donc)
// Elle contient aussi une liste de monstres.
// Toutes les listes devant être utilisées au sein du jeu devront être instanciées ici. C'est un peu le manager du jeu

package client.terrain;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Predicate;

import affichage.JeuVM;
import client.chateau.Chateau;
import client.monstre.GestionMonstre;
import client.monstre.GestionVagues;
import client.monstre.Monstre;
import client.monstre.Vague;
import client.physique.GestionCollision;
import client.physique.Position;
import client.score.GestionScore;
import client.score.Score;
import client.temps.Temps;
import client.tour.GestionTour;
import client.tour.Tour;
import client.libSong.LibSong;
import client.argent.Argent;
import client.argent.GestionArgent;
import client.chat.GestionChat;


public class Terrain {

	GestionMonstre gestionMonstre;
	GestionTour gestionTour;
	GestionArgent gestionArgent;
	GestionVagues gestionVagues;
	GestionCollision gestionCollision;
	GestionCases gestionCases;
	GestionScore gestionScore;
	
	Timer timer;
	Chateau chateau;
	boolean peutRetirerVie;
	
	static boolean aChange;
	static Temps temps;
	final static int tailleTerrain = 40;
	
	private int id = 0;
	
	public Terrain() {
		//this.chateau = new Chateau(1000, new Position(this.getGestionCases().getListeCases().get(this.getGestionCases().getListeCases().size() - 1).getPosition().getX(), this.getGestionCases().getListeCases().get(this.getGestionCases().getListeCases().size() - 1).getPosition().getY()));
		this.peutRetirerVie = true;
		this.gestionMonstre = new GestionMonstre(new Position(Case.getLongueur() * 2, Case.getLargeur() * 2));
		this.gestionTour = new GestionTour();
		this.gestionVagues = new GestionVagues();
		this.gestionArgent = new GestionArgent(new Argent(100));
		this.gestionCollision = new GestionCollision();
		this.gestionCases = new GestionCases(); 
		this.gestionScore = new GestionScore(new Score(0));
		this.temps = new Temps();
		this.setaChange(true);
		this.chateau = new Chateau(1000, new Position(616, 459));
	}
	
	// Procédure pour initialiser un nouveau terrain
	public void initialiserTerrain(int longueurFenetre, int largeurFenetre, int longueurRect, int largeurRect) {
		gestionCases.initialiserCases(longueurFenetre, largeurFenetre, longueurRect, largeurRect);
		initialiserChateau();
		initialiserGestionMonstre();
		initialiserGestionArgent();
		getGestionVagues().preparerVagues();
		
	}
	
	// Permet d'initialiser un chateau sur la dernière case du jeu (en bas à droite normalement)
	public void initialiserChateau() {
		gestionCases.getListeCases().get(gestionCases.getListeCases().size() - 1).setMonChateau(chateau);
		gestionCases.getListeCases().get(gestionCases.getListeCases().size() - 1).setOccupe(true);
	}
	
	// Permet d'initialiser la gestion des monstres
	public void initialiserGestionMonstre() {
		gestionMonstre = new GestionMonstre(new Position(Case.getLongueur() * 2, Case.getLargeur() * 2));
	} 

	// Permet d'effectuer les actions sur le terrain
	public void jouer(boolean multi) {
		if(!multi) {
			gestionVagues.actualiserVague(this);
			gestionMonstre.verifierNombreMonstres(this);
		}
		gestionMonstre.deplacerMonstres(this);
		gestionMonstre.actualiserEtatMonstres(this);
		gestionTour.actualiserEtatTours();
		gestionCollision.actualiserCollisions(this);
		gestionCases.actualiserEtatCases(this);
		
	}
	
	// Permet de gérer un click, sans que ça click des milliers de fois (utilisation d'un timer)
	public void gestionClique() {
		if (isPeutRetirerVie()) {
			chateau.retirerVie(5);
			setPeutRetirerVie(false);
			ReminderBeep(50);
		}
	}
	
	// Fonction/Classe permettant de faire un timer
	public void ReminderBeep(int time) {
		timer = new Timer();
		timer.schedule(new RemindTask(), time * 100);
	}
	class RemindTask extends TimerTask {
		public void run() {
			System.out.println("Time's up!");
			setPeutRetirerVie(true);
			timer.cancel();
    	}
 	}

	public boolean isPeutRetirerVie() {
		return peutRetirerVie;
	}

	public void setPeutRetirerVie(boolean peutRetirerVie) {
		this.peutRetirerVie = peutRetirerVie;
	}
	
	public GestionMonstre getGestionMonstre() {
		return gestionMonstre;
	}
	
	public GestionTour getGestionTour() {
		return gestionTour;
	}
	
	public GestionScore getGestionScore() {
		return gestionScore;
	}

	public static boolean isaChange() {
		return aChange;
	}

	public void setaChange(boolean aChange) {
		Terrain.aChange = aChange;
	}
	
	// Permet d'initialiser la gestion de l'argent
	public void initialiserGestionArgent() {
		gestionArgent = new GestionArgent(new Argent(100));
	}
	
	public GestionArgent getGestionArgent() {
		return gestionArgent;
	}

	public static Temps getTemps() {
		return temps;
	}

	public static void setTemps(Temps temps) {
		Terrain.temps = temps;
	}

	public GestionVagues getGestionVagues() {
		return gestionVagues;
	}

	public void setGestionVagues(GestionVagues gestionVagues) {
		this.gestionVagues = gestionVagues;
	}

	public GestionCases getGestionCases() {
		return gestionCases;
	}

	public void setGestionCases(GestionCases gestionCases) {
		this.gestionCases = gestionCases;
	}

	public Chateau getChateau() {
		return chateau;
	}

	public void setChateau(Chateau chateau) {
		this.chateau = chateau;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
