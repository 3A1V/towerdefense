package client.score;

public class Score {

	int score;
	boolean plusDargent;

	public Score(int score) {
		this.score = score;
	}
	

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}


	public void ajouterScore(int score) {
		this.score += score;
	}
}
