package client.score;

import client.tour.Tour;

public class GestionScore {

	Score monScore;

	public GestionScore(Score monScore) {
		this.monScore = monScore;
	}

	public Score getMonScore() {
		return monScore;
	}

	public void setMonScore(Score monScore) {
		this.monScore = monScore;
	}
	
	
	
}