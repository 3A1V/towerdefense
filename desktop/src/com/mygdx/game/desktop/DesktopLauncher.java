package com.mygdx.game.desktop;

import affichage.Jeu;
import affichage.JeuAD;
import affichage.JeuAE;
import affichage.JeuAI;
import affichage.JeuReseau;
import affichage.JeuVM;
import javafx.scene.layout.Border;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;


public class DesktopLauncher extends JFrame implements ActionListener {

	//Menu principal
	private JPanel panel;
	private JLabel titre;
	private JButton jouer;
	private JButton heberger;
	private JButton connecter;
	private JButton regle;
	private JButton credit;
	private GridLayout gl;
	private Font policeTitre;
	private Font police;
	
	//regle du jeu
	private JFrame regleDuJeu;
	private JPanel panelRegle;
	private JTextArea texteRegle;

	private JButton retourRegle;
	
	//Credit
	private JFrame frameCredit;
	private JPanel panelCredit;
	private JTextArea texteCredit;
	private JButton retourCredit;
	
	DesktopLauncher(){
		
	policeTitre = new Font("Calibri", Font.BOLD, 30);
	police = new Font("Calibri", Font.BOLD, 15);
	
	gl = new GridLayout(6, 1);
	gl.setVgap(60);
	
	panel = new JPanel(gl);
	panel.setBackground(new Color(255,255,255));
	
	titre = new JLabel("Tower Defense");
	titre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
	titre.setFont(policeTitre);
	
	jouer = new JButton("Jouer");
	jouer.setSize(150, 20);
	jouer.setFont(police);
	jouer.setBackground(new Color(255,255,255));
	jouer.addActionListener(this);
	
	heberger = new JButton("Heberger");
	heberger.setSize(150, 20);
	heberger.setFont(police);
	heberger.setBackground(new Color(255,255,255));
	heberger.addActionListener(this);
	
	connecter = new JButton("Se Connecter");
	connecter.setSize(150, 20);
	connecter.setFont(police);
	connecter.setBackground(new Color(255,255,255));
	connecter.addActionListener(this);
	
	regle = new JButton("R�gle");
	regle.setSize(150, 20);
	regle.setFont(police);
	regle.setBackground(new Color(255,255,255));
	regle.addActionListener(this);
	
	credit = new JButton("Cr�dit");
	credit.setSize(150, 20);
	credit.setFont(police);
	credit.setBackground(new Color(255,255,255));
	credit.addActionListener(this);
	
	panel.add(titre);
	panel.add(jouer);
	panel.add(heberger);
	panel.add(connecter);
	panel.add(regle);
	panel.add(credit);
	
	add(panel);
	setDefaultCloseOperation(EXIT_ON_CLOSE);
	setSize(500, 500);
	setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object o = e.getSource();
		System.out.println("evenement");
		if(o == regle){
			System.out.println("if regle");
			regleDuJeu = new JFrame();
			panelRegle = new JPanel();
			panelRegle.setBackground(new Color(255,255,255));
			retourRegle = new JButton("Menu Principal");
			retourRegle.setFont(police);
			retourRegle.setBackground(new Color(255,255,255));
			retourRegle.addActionListener(this);
			
			texteRegle = new JTextArea("\n Tuez tout les montres avant qu'ils n'atteignent votre château. \n Vous pouvez changer le type de tour que vous posez en  suivant les \n commandes ci-dessous : \n \n"
					+ "W : 100 PV et 20 d'Attaque \n"
					+ "X : 1000PV et 200 d'Attaque \n"
					+ "C : 600 PV et 600 d'Attaque \n"); 
			texteRegle.setSize(500, 500);
			texteRegle.setFont(police);
			
			
			panelRegle.add(retourRegle);
			panelRegle.add(texteRegle);
			
			regleDuJeu.add(panelRegle);
			regleDuJeu.setDefaultCloseOperation(EXIT_ON_CLOSE);
			regleDuJeu.setSize(500,500);
			regleDuJeu.setVisible(true);
		}
		
		if(o == retourRegle){
			System.out.println("retour regle");
			regleDuJeu.dispose();
		}
		
		if(o == credit){
			System.out.println("if credit");
			frameCredit = new JFrame();
			panelCredit = new JPanel();
			panelCredit.setBackground(new Color(255,255,255));
			texteCredit = new JTextArea(" \n Andy EFOMI, Alexandre ISSA, Arsène DUBOIS, Valentin MERCADIER");
			texteCredit.setFont(police);
			retourCredit = new JButton("Menu Principal");
			retourCredit.setBackground(new Color(255,255,255));
			retourCredit.setFont(police);
			retourCredit.addActionListener(this);
			
			panelCredit.add(retourCredit, BorderLayout.NORTH);
			panelCredit.add(texteCredit, BorderLayout.SOUTH);
			
			frameCredit.add(panelCredit);
			frameCredit.setDefaultCloseOperation(EXIT_ON_CLOSE);
			frameCredit.setSize(500, 500);
			frameCredit.setVisible(true);
		}
		
		if(o == retourCredit){
			System.out.println("retour credit");
			frameCredit.dispose();
		}
		
		if(o == jouer){
			LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
			config.resizable = false;
			new LwjglApplication(new JeuAI(), config);
			this.dispose();
		}
		
		if(o == heberger){
			LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
			config.resizable = false;
			new LwjglApplication(new JeuReseau(true), config);
			this.dispose();
		}
		
		if(o == connecter){
			LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
			config.resizable = false;
			new LwjglApplication(new JeuReseau(false), config);
			this.dispose();
		}
		
		
	}
	
	
	public static void main (String args[]){
		DesktopLauncher desktop = new DesktopLauncher();
	}
}
